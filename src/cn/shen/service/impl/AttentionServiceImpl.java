package cn.shen.service.impl;

import cn.shen.entity.Attention;
import cn.shen.mapper.AttentionMapper;
import cn.shen.service.AttentionService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>

 */
@Service
public class AttentionServiceImpl extends ServiceImpl<AttentionMapper, Attention> implements AttentionService {

}
