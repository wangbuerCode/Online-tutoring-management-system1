package cn.shen.service.impl;

import cn.shen.entity.Need;
import cn.shen.mapper.NeedMapper;
import cn.shen.service.NeedService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class NeedServiceImpl extends ServiceImpl<NeedMapper, Need> implements NeedService {

}
