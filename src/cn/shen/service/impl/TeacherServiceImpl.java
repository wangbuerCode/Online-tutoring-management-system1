package cn.shen.service.impl;

import cn.shen.entity.Teacher;
import cn.shen.mapper.TeacherMapper;
import cn.shen.service.TeacherService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {

}
