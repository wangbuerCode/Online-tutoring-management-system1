package cn.shen.service.impl;

import cn.shen.entity.Message;
import cn.shen.mapper.MessageMapper;
import cn.shen.service.MessageService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {

}
