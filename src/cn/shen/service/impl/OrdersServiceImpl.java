package cn.shen.service.impl;

import cn.shen.entity.Orders;
import cn.shen.mapper.OrdersMapper;
import cn.shen.service.OrdersService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

}
