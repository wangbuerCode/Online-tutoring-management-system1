package cn.shen.service.impl;

import cn.shen.entity.Chart;
import cn.shen.mapper.ChartMapper;
import cn.shen.service.ChartService;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 */
@Service
public class ChartServiceImpl extends ServiceImpl<ChartMapper, Chart> implements ChartService {

}
