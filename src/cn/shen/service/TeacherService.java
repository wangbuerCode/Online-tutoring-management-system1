package cn.shen.service;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.shen.entity.Teacher;


public interface TeacherService extends IService<Teacher> {

}
