package cn.shen.service;


import com.baomidou.mybatisplus.extension.service.IService;

import cn.shen.entity.Reply;


public interface ReplyService extends IService<Reply> {
}
