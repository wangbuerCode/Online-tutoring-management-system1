package cn.shen.service;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.shen.entity.Message;

public interface MessageService extends IService<Message> {

}
