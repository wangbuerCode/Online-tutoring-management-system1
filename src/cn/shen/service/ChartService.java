package cn.shen.service;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.shen.entity.Chart;

public interface ChartService extends IService<Chart> {

}
