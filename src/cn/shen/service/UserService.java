package cn.shen.service;

import com.baomidou.mybatisplus.extension.service.IService;

import cn.shen.entity.User;


public interface UserService extends IService<User> {

}
