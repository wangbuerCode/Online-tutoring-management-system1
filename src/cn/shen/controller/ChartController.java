package cn.shen.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mysql.jdbc.StringUtils;

import cn.shen.entity.Teacher;
import cn.shen.entity.User;
import cn.shen.service.AttentionService;
import cn.shen.service.TeacherService;
import cn.shen.service.UserService;

/**
 * <p>
 * 前端控制器
 * </p>
 */
@Controller
@RequestMapping("/chart")
public class ChartController {
	@Autowired
	AttentionService attentionService;

	@Autowired
	UserService userService;

	@Autowired
	TeacherService teacherService;

	// 通过ID查询
	@RequestMapping("getById")
	public String getById( String teacherId, String studentId, HttpServletRequest request) {
		if (!StringUtils.isNullOrEmpty(teacherId)) {
			User user = userService.getById(studentId);
			Teacher teacher = teacherService.getById(teacherId);
			request.setAttribute("user", user);
			request.setAttribute("teacherId", teacherId);
			System.out.println("++++////////////////++++++++++"+user);
			System.out.println(teacherId);
			System.out.println(teacher);
			return "front/jsp/chart";
		} else {
			return "front/jsp/userAttention";
		}

	}
	// 通过ID查询
	@RequestMapping("getByt_Id")
	public String getByt_Id( String teacherId, String studentId, HttpServletRequest request) {
		if (!StringUtils.isNullOrEmpty(teacherId)) {
			Teacher teacher = teacherService.getById(teacherId);
			request.setAttribute("teacherId", teacherId);
			System.out.println(teacherId);
			System.out.println(teacher);
			return "front/jsp/t_chart";
		} else {
			return "front/jsp/teacherAttention";
		}

	}

}
