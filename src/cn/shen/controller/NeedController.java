package cn.shen.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.shen.entity.Need;
import cn.shen.entity.User;
import cn.shen.service.NeedService;
import cn.shen.service.UserService;
import cn.shen.tools.Constants;
import cn.shen.tools.PageSupport;
import cn.shen.tools.ServerResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mysql.jdbc.StringUtils;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Controller
@RequestMapping("/need")
public class NeedController {
	@Autowired
	NeedService needService;
	
	@Autowired
	UserService userService;
	
	//分页查询
	@RequestMapping("list")
	public String list(String name,String grade,String subject,Integer pageIndex,HttpServletRequest request){
        List<Need> needList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        
        
        //需求查询条件
        QueryWrapper<Need> wrapper=new QueryWrapper<Need>();
        if(!StringUtils.isNullOrEmpty(name)){
        	QueryWrapper<User> Uwrapper=new QueryWrapper<User>();
        	Uwrapper.like("name",name);
        	List<User> list=userService.list(Uwrapper);
        	List<Object> uid=new ArrayList<Object>();
        	for(int i=0;i<list.size();i++){
            	uid.add(list.get(i).getId());
            }
        	wrapper.in("uid", uid);
        }
        if(!StringUtils.isNullOrEmpty(grade)){
            wrapper.like("grade",grade);
        }
        if(!StringUtils.isNullOrEmpty(subject)){
            wrapper.like("subject",subject);
        }
        wrapper.eq("isdel", 0);
        wrapper.orderByDesc("id");
        //产生page对象，传递当前页码和每页显示多少
        IPage<Need> page_list=new Page<Need>(pageIndex,pageSize);

        page_list=needService.page(page_list,wrapper);

        needList = page_list.getRecords();//得到集合
        
        for(int i=0;i<needList.size();i++){
        	needList.get(i).setUserName(userService.getById(needList.get(i).getUid()).getName());
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("needList", needList);
        request.setAttribute("name", name);
        request.setAttribute("grade", grade);
        request.setAttribute("subject", subject);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "manage/jsp/needlist";
    }
	
	//通过ID查询需求
	@RequestMapping("getById")
	public String getById(String method,String id,HttpServletRequest request){
		 if(method.equals("view")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Need need = needService.getById(id);
	                need.setUserName(userService.getById(need.getUid()).getName());
	                request.setAttribute("need", need);
	                return "manage/jsp/needview";
	            }else{
	                return "need/list";
	            }
	        }else if(method.equals("modify")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Need need = needService.getById(id);
	                request.setAttribute("need", need);
	                return "manage/jsp/needmodify";
	            }else{
	                return "need/list";
	            }
	        }else{
	            return "need/list";
	        }
	}

	//需求注册
	@RequestMapping("add")
	@ResponseBody
	public ServerResponse add(Need need){
		Date today=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
		String time=sdf.format(today);
		
		need.setTime(time);
		need.setIsdel(0);
		
		if(needService.save(need)){
			return  new ServerResponse("0", "发布成功!");
        }else{
        	return  new ServerResponse("1", "发布失败!");
        }
	}

	//需求修改
	@RequestMapping("modify")
	@ResponseBody
	public ServerResponse modify(Need need){
		
		if(needService.updateById(need)){
			return  new ServerResponse("0", "修改成功!");
        }else{
        	return  new ServerResponse("1", "修改失败!");
        }
	}
	
	//需求删除
	@RequestMapping("del")
	@ResponseBody
	public ServerResponse del(Integer id){
		Need need=needService.getById(id);
		if(need!=null){
			need.setIsdel(1);
			if(needService.updateById(need)){
				return  new ServerResponse("0", "删除成功!");
			}else{
				return  new ServerResponse("1", "删除失败!");
			}
		}else{
			return  new ServerResponse("1", "该需求没有!");
		}
	}

	//前台老师查询学生发布的需求
	@RequestMapping("qianList")
	public String qianList(String grade,String subject,Integer pageIndex,HttpServletRequest request){
        List<Need> needList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        //需求查询条件
        QueryWrapper<Need> wrapper=new QueryWrapper<Need>();
        if(!StringUtils.isNullOrEmpty(grade)){
            wrapper.like("grade",grade);
        }
        if(!StringUtils.isNullOrEmpty(subject)){
            wrapper.like("subject",subject);
        }
        wrapper.eq("isdel", 0);
        wrapper.orderByDesc("id");
        //产生page对象，传递当前页码和每页显示多少
        IPage<Need> page_list=new Page<Need>(pageIndex,pageSize);

        page_list=needService.page(page_list,wrapper);

        needList = page_list.getRecords();//得到集合
        
        for(int i=0;i<needList.size();i++){
        	needList.get(i).setUserName(userService.getById(needList.get(i).getUid()).getName());
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("needList", needList);
        request.setAttribute("grade", grade);
        request.setAttribute("subject", subject);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "front/jsp/teacher";
    }

	//学生查询自己的发布
	@RequestMapping("userList")
	public String userList(Integer uid, String grade, String subject,
			Integer pageIndex, HttpServletRequest request) {
		List<Need> needList = null;
		// 设置页面容量
		int pageSize = Constants.pageSize;
		if (pageIndex == null) {
			pageIndex = 1;
		}

		// 需求查询条件
		QueryWrapper<Need> wrapper = new QueryWrapper<Need>();
		if (!StringUtils.isNullOrEmpty(grade)) {
			wrapper.like("grade", grade);
		}
		if (!StringUtils.isNullOrEmpty(subject)) {
			wrapper.like("subject", subject);
		}
		wrapper.eq("isdel", 0);
		wrapper.eq("uid", uid);
		wrapper.orderByDesc("id");
		// 产生page对象，传递当前页码和每页显示多少
		IPage<Need> page_list = new Page<Need>(pageIndex, pageSize);

		page_list = needService.page(page_list, wrapper);

		needList = page_list.getRecords();// 得到集合


		// 总数量（表）
		int totalCount = (int) page_list.getTotal();

		// 总页数
		PageSupport pages = new PageSupport();
		pages.setCurrentPageNo(pageIndex);
		pages.setPageSize(pageSize);
		pages.setTotalCount(totalCount);

		int totalPageCount = pages.getTotalPageCount();

		// 控制首页和尾页
		if (pageIndex < 1) {
			pageIndex = 1;
		} else if (pageIndex > totalPageCount) {
			pageIndex = totalPageCount;
		}

		request.setAttribute("needList", needList);
		request.setAttribute("grade", grade);
		request.setAttribute("subject", subject);
		request.setAttribute("totalPageCount", totalPageCount);
		request.setAttribute("totalCount", totalCount);
		request.setAttribute("currentPageNo", pageIndex);
		return "front/jsp/userNeedList";
	}
	
	//查询发布详情
		@RequestMapping("Content")
		public String Content(Integer id,HttpServletRequest request) {
			Need need=needService.getById(id);
			need.setUserName(userService.getById(need.getUid()).getName());
			
			if(need!=null){
				request.setAttribute("need", need);
				return "front/jsp/needview";
			}else{
				return "front/index.jsp";
			}
		}
}

