package cn.shen.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.shen.entity.Message;
import cn.shen.entity.Reply;
import cn.shen.entity.Teacher;
import cn.shen.entity.User;
import cn.shen.service.MessageService;
import cn.shen.service.ReplyService;
import cn.shen.service.TeacherService;
import cn.shen.service.UserService;
import cn.shen.tools.Constants;
import cn.shen.tools.PageSupport;
import cn.shen.tools.ServerResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mysql.jdbc.StringUtils;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Controller
@RequestMapping("/message")
public class MessageController {
	@Autowired
	MessageService messageService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	TeacherService teacherService;
	
	@Autowired
	ReplyService replyService;
	
	//分页查询
	@RequestMapping("list")
	public String list(String name,Integer pageIndex,HttpServletRequest request){
        List<Message> messageList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        
        
        //需求查询条件
        QueryWrapper<Message> wrapper=new QueryWrapper<Message>();
        if(!StringUtils.isNullOrEmpty(name)){
        	QueryWrapper<User> Uwrapper=new QueryWrapper<User>();
        	Uwrapper.like("name",name);
        	List<User> list=userService.list(Uwrapper);
        	List<Object> uid=new ArrayList<Object>();
        	for(int i=0;i<list.size();i++){
            	uid.add(list.get(i).getId());
            }
        	wrapper.in("uid", uid);
        }
        wrapper.eq("isdel", 0);
        wrapper.orderByDesc("id");
        //产生page对象，传递当前页码和每页显示多少
        IPage<Message> page_list=new Page<Message>(pageIndex,pageSize);

        page_list=messageService.page(page_list,wrapper);

        messageList = page_list.getRecords();//得到集合
        
        for(int i=0;i<messageList.size();i++){
        	messageList.get(i).setUserName(userService.getById(messageList.get(i).getUid()).getName());
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("messageList", messageList);
        request.setAttribute("name", name);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "manage/jsp/messagelist";
    }
	
	//后台管理员的留言查询
	@RequestMapping("getMyHou")
	public String getMyHou(Integer aid,String name,Integer pageIndex,HttpServletRequest request){
		List<Message> messageList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        
        
        //需求查询条件
        QueryWrapper<Message> wrapper=new QueryWrapper<Message>();
        if(!StringUtils.isNullOrEmpty(name)){
        	QueryWrapper<User> Uwrapper=new QueryWrapper<User>();
        	Uwrapper.like("name",name);
        	List<User> list=userService.list(Uwrapper);
        	List<Object> uid=new ArrayList<Object>();
        	for(int i=0;i<list.size();i++){
            	uid.add(list.get(i).getId());
            }
        	wrapper.in("uid", uid);
        }
        wrapper.eq("to_aid", aid);
        wrapper.eq("isdel", 0);
        wrapper.orderByDesc("id");
        
        //产生page对象，传递当前页码和每页显示多少
        IPage<Message> page_list=new Page<Message>(pageIndex,pageSize);

        page_list=messageService.page(page_list,wrapper);

        messageList = page_list.getRecords();//得到集合
        
        for(int i=0;i<messageList.size();i++){
        	messageList.get(i).setUserName(userService.getById(messageList.get(i).getUid()).getName());
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("messageList", messageList);
        request.setAttribute("name", name);
        request.setAttribute("aid", aid);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "/manage/jsp/mymessage";
	}
	
	//通过ID查询留言
	@RequestMapping("getById")
	public String getById(String method,String id,HttpServletRequest request){
		 if(method.equals("view")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Message message = messageService.getById(id);
	                message.setUserName(userService.getById(message.getUid()).getName());
	                if(message.getTo_tid()!=null){
	                	Teacher teacher=teacherService.getById(message.getTo_tid());
	                	request.setAttribute("teacherName", teacher.getName());
	                }else if(message.getTo_aid()!=null){
	                	User user=userService.getById(message.getTo_aid());
	                	request.setAttribute("adminName", user.getName());
	                }
	                request.setAttribute("message", message);
	                return "manage/jsp/messageview";
	            }else{
	                return "message/list";
	            }
	        }else if(method.equals("modify")){
	            if(!StringUtils.isNullOrEmpty(id)){
	            	Message message = messageService.getById(id);
	                message.setUserName(userService.getById(message.getUid()).getName());
	                if(message.getTo_tid()!=null){
	                	Teacher teacher=teacherService.getById(message.getTo_tid());
	                	request.setAttribute("teacherName", teacher.getName());
	                }else if(message.getTo_aid()!=null){
	                	User user=userService.getById(message.getTo_aid());
	                	request.setAttribute("adminName", user.getName());
	                }
	                request.setAttribute("message", message);
	                return "manage/jsp/messagemodify";
	            }else{
	                return "message/list";
	            }
	        }else if(method.equals("reply")){
	        	if(!StringUtils.isNullOrEmpty(id)){
	                Message message = messageService.getById(id);
	                message.setUserName(userService.getById(message.getUid()).getName());
	                if(message.getTo_tid()!=null){
	                	Teacher teacher=teacherService.getById(message.getTo_tid());
	                	request.setAttribute("teacherName", teacher.getName());
	                }else if(message.getTo_aid()!=null){
	                	User user=userService.getById(message.getTo_aid());
	                	request.setAttribute("adminName", user.getName());
	                }
	                
	                QueryWrapper<Reply> wrapper=new QueryWrapper<Reply>();
	        		wrapper.eq("mid",message.getId());
	        		wrapper.eq("isdel", 0);
	        		List<Reply> list=replyService.list(wrapper);
	        		Reply reply=null;
	        		for(int i=0;i<list.size();i++){
	        			if(list.size()>0){
	        				reply=list.get(i);
	        			}
	        		}
	                
	                request.setAttribute("message", message);
	                request.setAttribute("reply", reply);
	                return "manage/jsp/replyadd";
	            }else{
	                return "message/mymessage";
	            }
			}else{
	            return "message/list";
	        }
	}

	//留言增加
	@RequestMapping("add")
	@ResponseBody
	public ServerResponse add(Message message){
				
		Date today=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=sdf.format(today);
		
		message.setTime(time);
		message.setIsdel(0);
		message.setIsreply(0);
		if(messageService.save(message)){
			return  new ServerResponse("0", "留言成功!");
        }else{
        	return  new ServerResponse("1", "留言失败!");
        }
	}
	
	//管理员留言增加
	@RequestMapping("adminAdd")
	@ResponseBody
	public ServerResponse adminAdd(Message message) {

		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = sdf.format(today);
		
		if(message.getTo_aid()!=null&&message.getTo_aid()!=0){
			message.setTime(time);
			message.setIsdel(0);
			message.setIsreply(0);
			if (messageService.save(message)) {
				return new ServerResponse("0", "留言成功!");
			} else {
				return new ServerResponse("1", "留言失败!");
			}
		}else{
			return new ServerResponse("1", "请选择留言的管理员!");
		}
	}

	//留言修改
	@RequestMapping("modify")
	@ResponseBody
	public ServerResponse modify(Message message){
		
		if(messageService.updateById(message)){
			return  new ServerResponse("0", "修改成功!");
        }else{
        	return  new ServerResponse("1", "修改失败!");
        }
	}
	
	//留言删除
	@RequestMapping("del")
	@ResponseBody
	public ServerResponse del(Integer id,HttpServletResponse response){
		Message message=messageService.getById(id);
		if(message!=null){
			message.setIsdel(1);
			if(messageService.updateById(message)){
				return  new ServerResponse("0", "删除成功!");
			}else{
				return  new ServerResponse("1", "删除失败!");
			}
		}else{
			return  new ServerResponse("1", "该用户不存在!");
		}
	}

	//用户的留言，根据用户的id查询
	@RequestMapping("getUerMessage")
	public String getUerMessage(Integer uid,Integer pageIndex,HttpServletRequest request){
			List<Message> messageList = null;
	        //设置页面容量
	        int pageSize = Constants.pageSize;
	        if (pageIndex == null) {
	            pageIndex = 1;
	        }
	        //需求查询条件
	        QueryWrapper<Message> wrapper=new QueryWrapper<Message>();
	        wrapper.eq("uid", uid);
	        wrapper.eq("isdel", 0);
	        wrapper.orderByDesc("id");
	        
	        //产生page对象，传递当前页码和每页显示多少
	        IPage<Message> page_list=new Page<Message>(pageIndex,pageSize);

	        page_list=messageService.page(page_list,wrapper);

	        messageList = page_list.getRecords();//得到集合
	        
	        for(int i=0;i<messageList.size();i++){
	        	if(messageList.get(i).getTo_aid()!=null){
	        		messageList.get(i).setAdminName(userService.getById(messageList.get(i).getTo_aid()).getName());
	        	}else if(messageList.get(i).getTo_tid()!=null){
	        		messageList.get(i).setTeacherName(teacherService.getById(messageList.get(i).getTo_tid()).getName());
	        	}
	        }

	        //总数量（表）
	        int totalCount = (int)page_list.getTotal();

	        //总页数
	        PageSupport pages = new PageSupport();
	        pages.setCurrentPageNo(pageIndex);
	        pages.setPageSize(pageSize);
	        pages.setTotalCount(totalCount);

	        int totalPageCount = pages.getTotalPageCount();

	        //控制首页和尾页
	        if (pageIndex < 1) {
	            pageIndex = 1;
	        } else if (pageIndex > totalPageCount) {
	            pageIndex = totalPageCount;
	        }

	        request.setAttribute("messageList", messageList);
	        request.setAttribute("totalPageCount", totalPageCount);
	        request.setAttribute("totalCount", totalCount);
	        request.setAttribute("currentPageNo", pageIndex);
	        return "front/jsp/userMessage";
		}

	// 用户的留言，根据老师的id查询
	@RequestMapping("getTeacherMessage")
	public String getTeacherMessage(Integer tid, String name, Integer pageIndex,
			HttpServletRequest request) {
		List<Message> messageList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        //需求查询条件
        QueryWrapper<Message> wrapper=new QueryWrapper<Message>();
        wrapper.eq("to_tid", tid);
        wrapper.eq("isdel", 0);
        wrapper.orderByDesc("id");
        
        //产生page对象，传递当前页码和每页显示多少
        IPage<Message> page_list=new Page<Message>(pageIndex,pageSize);

        page_list=messageService.page(page_list,wrapper);

        messageList = page_list.getRecords();//得到集合
        
        for(int i=0;i<messageList.size();i++){
        	if(messageList.get(i).getTo_aid()!=null){
        		messageList.get(i).setAdminName(userService.getById(messageList.get(i).getTo_aid()).getName());
        	}else if(messageList.get(i).getTo_tid()!=null){
        		messageList.get(i).setTeacherName(teacherService.getById(messageList.get(i).getTo_tid()).getName());
        	}
        	messageList.get(i).setUserName(userService.getById(messageList.get(i).getUid()).getName());
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("messageList", messageList);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "front/jsp/teacherMessage";
	}
	
	//留言删除
	@RequestMapping("qianDel")
	@ResponseBody
	public ServerResponse qianDel(Integer id){
		Message message=messageService.getById(id);
		if(message!=null){
			message.setIsdel(1);
			if(messageService.updateById(message)){
				return  new ServerResponse("0", "删除成功!");
			}else{
				return  new ServerResponse("1", "删除失败!");
			}
		}else{
			return  new ServerResponse("1", "该留言不存在!");
		}
	}
	
	// 用户查看留言详情
	@RequestMapping("userGetContent")
	public String userGetContent(Integer id, HttpServletRequest request) {
		Message message=messageService.getById(id);
		Reply reply=null;
		QueryWrapper<Reply> wrapper=new QueryWrapper<Reply>();
		
		if(message.getIsreply()==1&&message.getTo_tid()!=null){
		    wrapper.eq("isdel", 0);
		    wrapper.eq("mid", message.getId());
		    wrapper.eq("isadmin", 0);
		}else if(message.getIsreply()==1&&message.getTo_aid()!=null){
		    wrapper.eq("isdel", 0);
		    wrapper.eq("mid", message.getId());
		    wrapper.eq("isadmin", 1);
		}
		
		
		List<Reply> list=replyService.list(wrapper);
		
		if(list.size()>0){
			for (int i = 0; i < list.size(); i++) {
				reply=list.get(i);
				if(reply.getUid()!=null){
					reply.setUserName(userService.getById(message.getUid()).getName());
		    	}else if(reply.getRid()!=null&&reply.getIsadmin()==0){
		    		reply.setReplyName(teacherService.getById(reply.getRid()).getName());
		    	}else if(reply.getRid()!=null&&reply.getIsadmin()==1){
		    		reply.setReplyName(userService.getById(reply.getRid()).getName());
		    	}
			}
		}
        
		if(message.getTo_aid()!=null){
			message.setAdminName(userService.getById(message.getTo_aid()).getName());
    	}else if(message.getTo_tid()!=null){
    		message.setTeacherName(teacherService.getById(message.getTo_tid()).getName());
    	}
		
		reply.setUserName(userService.getById(message.getUid()).getName());

		if (message != null) {
			request.setAttribute("message", message);
			request.setAttribute("reply", reply);
			return "front/jsp/userMessageview";
		} else {
			return "redirect:/qjy/message/getUerReply";
		}
	}
	
	//老师查看留言详情
		@RequestMapping("teacherGetContent")
		public String teacherGetContent(Integer id, HttpServletRequest request) {
			Message message = messageService.getById(id);
			message.setUserName(userService.getById(message.getUid()).getName());
			
			if (message != null) {
				request.setAttribute("message", message);
				return "front/jsp/teacherMessageview";
			} else {
				return "redirect:/qjy/message/getUerReply";
			}
		}
}

