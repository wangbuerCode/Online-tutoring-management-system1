package cn.shen.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.shen.entity.Teacher;
import cn.shen.entity.User;
import cn.shen.service.TeacherService;
import cn.shen.service.UserService;
import cn.shen.tools.ServerResponse;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Controller
@RequestMapping("/uppwd") 
public class UppwdController {
	@Autowired
	UserService userService;
	
	@Autowired
	TeacherService teacherService;
	
	
	//用户修改密码
	@RequestMapping("uppwd")
	@ResponseBody
	public ServerResponse uppwd(Integer id,String passWord,String newPassWord,String reNewPassWord,Integer role){
		if(role==1){
			User user=userService.getById(id);
			if(user!=null){
				if(passWord.equals(user.getPassWord())){
					if(newPassWord.equals(reNewPassWord)){
						user.setPassWord(newPassWord);
						if(userService.updateById(user)){
							return  new ServerResponse("0", "修改密码成功!");
						}else{
							return  new ServerResponse("1", "修改密码失败!");
						}
					}else{
						return  new ServerResponse("1", "两次密码输入不相符!");
					}
				}else{
					return  new ServerResponse("1", "旧密码输入错误!");
				}
			}else{
				return  new ServerResponse("1", "该用户不存在!");
			}
        }else if(role==2){
        	Teacher teacher=teacherService.getById(id);
			if(teacher!=null){
				if(passWord.equals(teacher.getPassWord())){
					if(newPassWord.equals(reNewPassWord)){
						teacher.setPassWord(newPassWord);
						if(teacherService.updateById(teacher)){
							return  new ServerResponse("0", "修改密码成功!");
						}else{
							return  new ServerResponse("1", "修改密码失败!");
						}
					}else{
						return  new ServerResponse("1", "两次密码输入不相符!");
					}
				}else{
					return  new ServerResponse("1", "旧密码输入错误!");
				}
			}else{
				return  new ServerResponse("1", "该用户不存在!");
			}
        }else{
        	return  new ServerResponse("1", "修改密码失败！");
        }
	}

	
}

