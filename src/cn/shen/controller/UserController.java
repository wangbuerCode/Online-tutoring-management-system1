package cn.shen.controller;


import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mysql.jdbc.StringUtils;
import cn.shen.entity.User;
import cn.shen.service.UserService;
import cn.shen.tools.Constants;
import cn.shen.tools.PageSupport;
import cn.shen.tools.ServerResponse;

/**
 * <p>
 * 前端控制器
 * </p>
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserService userService;

	// 分页查询
	@RequestMapping("list")
	public String list(String name, Integer pageIndex, HttpServletRequest request) {
		List<User> userList = null;
		// 设置页面容量
		int pageSize = Constants.pageSize;
		if (pageIndex == null) {
			pageIndex = 1;
		}

		// 条件
		QueryWrapper<User> wrapper = new QueryWrapper<User>();
		if (!StringUtils.isNullOrEmpty(name)) {
			wrapper.like("name", name);
		}
		wrapper.eq("role", 1);
		wrapper.eq("isdel", 0);

		// 产生page对象，传递当前页码和每页显示多少
		IPage<User> page_list = new Page<User>(pageIndex, pageSize);

		page_list = userService.page(page_list, wrapper);

		userList = page_list.getRecords();// 得到集合

		// 总数量（表）
		int totalCount = (int) page_list.getTotal();

		// 总页数
		PageSupport pages = new PageSupport();
		pages.setCurrentPageNo(pageIndex);
		pages.setPageSize(pageSize);
		pages.setTotalCount(totalCount);

		int totalPageCount = pages.getTotalPageCount();

		// 控制首页和尾页
		if (pageIndex < 1) {
			pageIndex = 1;
		} else if (pageIndex > totalPageCount) {
			pageIndex = totalPageCount;
		}

		request.setAttribute("userList", userList);
		request.setAttribute("name", name);
		request.setAttribute("totalPageCount", totalPageCount);
		request.setAttribute("totalCount", totalCount);
		request.setAttribute("currentPageNo", pageIndex);
		return "manage/jsp/userlist";
	}

	// 通过ID查询用户
	@RequestMapping("getById")
	public String getById(String method, String id, HttpServletRequest request) {
		if (method.equals("view")) {
			if (!StringUtils.isNullOrEmpty(id)) {
				User user = userService.getById(id);
				request.setAttribute("role", user.getRole());
				request.setAttribute("user", user);
				return "manage/jsp/userview";
			} else {
				return "user/list";
			}
		} else if (method.equals("modify")) {
			if (!StringUtils.isNullOrEmpty(id)) {
				User user = userService.getById(id);
				request.setAttribute("user", user);
				return "manage/jsp/usermodify";
			} else {
				return "user/list";
			}
		} else if (method.equals("uppwd")) {
			if (!StringUtils.isNullOrEmpty(id)) {
				User user = userService.getById(id);
				request.setAttribute("user", user);
				return "manage/uppwd";
			} else {
				return "redirect:/index.jsp";
			}
		} else if (method.equals("qian")) {
			if (!StringUtils.isNullOrEmpty(id)) {
				User user = userService.getById(id);
				request.setAttribute("user", user);
				return "front/jsp/personal";
			} else {
				return "front/index";
			}

		} else if (method.equals("qupdate")) {
			if (!StringUtils.isNullOrEmpty(id)) {
				User user = userService.getById(id);
				request.setAttribute("user", user);
				return "front/modify";
			} else {
				return "front/index";
			}

		} else if (method.equals("quppwd")) {
			if (!StringUtils.isNullOrEmpty(id)) {
				User user = userService.getById(id);
				request.setAttribute("user", user);
				return "front/uppwd";
			} else {
				return "front/index";
			}

		} else {
			return "user/list";
		}
	}

	// 修改密码
	@RequestMapping("uppwd")
	@ResponseBody
	public ServerResponse uppwd(Integer id, String oldPassWord, String newPassword, String reNewPassword) {
		User user = userService.getById(id);
		if (user != null) {
			if (oldPassWord.equals(user.getPassWord())) {
				if (newPassword.equals(reNewPassword)) {
					user.setPassWord(newPassword);
					if (userService.updateById(user)) {
						return new ServerResponse("0", "修改密码成功!");
					} else {
						return new ServerResponse("1", "修改密码失败!");
					}
				} else {
					return new ServerResponse("1", "两次密码输入不相符!");
				}
			} else {
				return new ServerResponse("1", "旧密码输入错误!");
			}
		} else {
			return new ServerResponse("1", "该用户不存在!");
		}
	}

	// 用户增加
	@RequestMapping("add")
	@ResponseBody
	public ServerResponse add(User user, String repassword) {
		if (repassword != null) {
			if (repassword.equals(user.getPassWord())) {
				user.setRole(1);
				user.setIsnotice(0);
				user.setIsdel(0);
				if (userService.save(user)) {
					return new ServerResponse("0", "注册成功!");
				} else {
					return new ServerResponse("1", "添加失败!");
				}
			} else {
				return new ServerResponse("1", "输入密码不相符!");
			}
		} else {
			return new ServerResponse("1", "请再输入一次密码!");
		}
	}

	// 用户修改
	@RequestMapping("modify")
	@ResponseBody
	public ServerResponse modify(User user) {
		if (userService.updateById(user)) {
			return new ServerResponse("0", "修改成功!");
		} else {
			return new ServerResponse("1", "修改失败!");
		}
	}

	// 用户删除
	@RequestMapping("del")
	@ResponseBody
	public ServerResponse del(Integer id) {
		User user = userService.getById(id);
		if (user != null) {
			user.setIsdel(1);
			if (userService.updateById(user)) {
				return new ServerResponse("0", "删除成功!");
			} else {
				return new ServerResponse("1", "删除失败!");
			}
		} else {
			return new ServerResponse("1", "该用户不存在!");
		}
	}

	// 通知弹出
	@RequestMapping("userNotice")
	@ResponseBody
	public ServerResponse userNotice(Integer id) {
		User user = userService.getById(id);
		if (user != null) {
			if (user.getIsnotice() == 1) {
				return new ServerResponse("0", "您有新的订单，请前往我的订单查看!");
			} else {
				return new ServerResponse("1", "无消息!");
			}
		} else {
			return new ServerResponse("1", "无消息!");
		}
	}

	// 查询所有的系统管理员
	@RequestMapping("adminList")
	@ResponseBody
	public List<User> adminList() {
		List<User> list = null;
		// 条件
		QueryWrapper<User> wrapper = new QueryWrapper<User>();
		wrapper.eq("role", 0);
		list = userService.list(wrapper);
		return list;
	}
}
