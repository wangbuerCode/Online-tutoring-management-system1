package cn.shen.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.shen.entity.Message;
import cn.shen.entity.Reply;
import cn.shen.entity.User;
import cn.shen.service.MessageService;
import cn.shen.service.ReplyService;
import cn.shen.service.TeacherService;
import cn.shen.service.UserService;
import cn.shen.tools.Constants;
import cn.shen.tools.PageSupport;
import cn.shen.tools.ServerResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mysql.jdbc.StringUtils;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Controller
@RequestMapping("/reply")
public class ReplyController {
	@Autowired
	ReplyService replyService;
	
	@Autowired
	MessageService messageService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	TeacherService teacherService;
	
	//分页查询
	@RequestMapping("list")
	public String list(String name,Integer pageIndex,HttpServletRequest request){
        List<Reply> replyList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        //需求查询条件
        QueryWrapper<Reply> wrapper=new QueryWrapper<Reply>();
        if(!StringUtils.isNullOrEmpty(name)){
        	QueryWrapper<User> Uwrapper=new QueryWrapper<User>();
        	Uwrapper.like("name",name);
        	List<User> list=userService.list(Uwrapper);
        	List<Object> uid=new ArrayList<Object>();
        	if(list.size()>0){
        		for(int i=0;i<list.size();i++){
                	uid.add(list.get(i).getId());
                }
        		QueryWrapper<Message> Mwrapper=new QueryWrapper<Message>();
            	Mwrapper.in("uid", uid);
            	List<Message> listM=messageService.list(Mwrapper);
            	List<Object> mid=new ArrayList<Object>();
            	for(int i=0;i<listM.size();i++){
            		mid.add(listM.get(i).getId());
                }
            	wrapper.in("mid", mid);
        	}else{
        		wrapper.in("mid", 0);
        	}
        }
        wrapper.eq("isdel", 0);
        wrapper.orderByDesc("id");
        //产生page对象，传递当前页码和每页显示多少
        IPage<Reply> page_list=new Page<Reply>(pageIndex,pageSize);

        page_list=replyService.page(page_list,wrapper);

        replyList = page_list.getRecords();//得到集合
        
        for(int i=0;i<replyList.size();i++){
        	replyList.get(i).setUserName(userService.getById(messageService.getById(replyList.get(i).getMid()).getUid()).getName());
        	if(replyList.get(i).getUid()!=null){
        		replyList.get(i).setReplyName(userService.getById(replyList.get(i).getUid()).getName());
        	}else if(replyList.get(i).getRid()!=null){
        		replyList.get(i).setReplyName(teacherService.getById(replyList.get(i).getRid()).getName());
        	}
        
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("replyList", replyList);
        request.setAttribute("name", name);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "manage/jsp/replylist";
    }
	
	//通过ID查询回复
	@RequestMapping("getById")
	public String getById(String method,String id,HttpServletRequest request){
		 if(method.equals("view")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Reply reply = replyService.getById(id);
	                if(reply.getUid()!=null){
	                	reply.setReplyName(userService.getById(reply.getUid()).getName());
	            	}else if(reply.getRid()!=null){
	            		reply.setReplyName(teacherService.getById(reply.getRid()).getName());
	            	}
	                reply.setUserName(userService.getById(messageService.getById(reply.getMid()).getUid()).getName());
	                request.setAttribute("reply", reply);
	                return "manage/jsp/replyview";
	            }else{
	                return "reply/list";
	            }
	        }else if(method.equals("modify")){
	            if(!StringUtils.isNullOrEmpty(id)){
	            	Reply reply = replyService.getById(id);
	            	if(reply.getUid()!=null){
	                	reply.setReplyName(userService.getById(reply.getUid()).getName());
	            	}else if(reply.getRid()!=null){
	            		reply.setReplyName(teacherService.getById(reply.getRid()).getName());
	            	}
	            	reply.setUserName(userService.getById(messageService.getById(reply.getMid()).getUid()).getName());
	                request.setAttribute("reply", reply);
	                return "manage/jsp/replymodify";
	            }else{
	                return "reply/list";
	            }
	        }else{
	            return "reply/list";
	        }
	}

	//回复增加
	@RequestMapping("add")
	@ResponseBody
	public ServerResponse add(Reply reply){
		Message message=messageService.getById(reply.getMid());
		if(message.getIsreply()==0){
			if(message.getTo_aid()!=null&&message.getTo_aid()!=0){
				reply.setIsadmin(1);
			}else if (message.getTo_tid()!=null&&message.getTo_tid()!=0) {
				reply.setIsadmin(0);
			}
			
			message.setIsreply(1);
			boolean b=messageService.updateById(message);
			
			Date today=new Date();
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String time=sdf.format(today);
			
			reply.setTime(time);
			reply.setIsdel(0);
			if(replyService.save(reply)&&b){
				return  new ServerResponse("0", "添加成功!");
	        }else{
	        	return  new ServerResponse("1", "添加失败!");
	        }
		}else{
			return  new ServerResponse("1", "该留言已经被回复!");
		}
	}

	//回复修改
	@RequestMapping("modify")
	@ResponseBody
	public ServerResponse modify(Integer id,String content){
		Reply reply=replyService.getById(id);
		reply.setContent(content);
		if(replyService.updateById(reply)){
			return  new ServerResponse("0", "修改成功!");
        }else{
        	return  new ServerResponse("1", "修改失败!");
        }
	}
	
	//回复删除
	@RequestMapping("del")
	@ResponseBody
	public ServerResponse del(Integer id){
		Reply reply=replyService.getById(id);
		if(reply!=null){
			reply.setIsdel(1);
			if(replyService.updateById(reply)){
				return  new ServerResponse("0", "删除成功!");
			}else{
				return  new ServerResponse("1", "删除失败!");
			}
		}else{
			return  new ServerResponse("1", "该用户不存在!");
		}
	}
	
	//根据用户的uid查询回复
	@RequestMapping("getUerReply")
	public String getUerReply(Integer uid,Integer pageIndex,HttpServletRequest request){
		 List<Reply> replyList = null;
	        //设置页面容量
	        int pageSize = Constants.pageSize;
	        if (pageIndex == null) {
	            pageIndex = 1;
	        }
	        
	        QueryWrapper<Reply> wrapper=new QueryWrapper<Reply>();
	        QueryWrapper<Message> Mwrapper=new QueryWrapper<Message>();
	        Mwrapper.eq("uid", uid);
	        List<Message> list=messageService.list(Mwrapper);
	        List<Object> mid=new ArrayList<Object>();
	        if(list.size()>0){
	        	for(int i=0;i<list.size();i++){
	        		mid.add(list.get(i).getId());
	        	}
	        }
	        wrapper.eq("isdel", 0);
	        wrapper.in("mid", mid);
	        wrapper.eq("isadmin", 0);
	        wrapper.orderByDesc("id");
        	
	        //产生page对象，传递当前页码和每页显示多少
	        IPage<Reply> page_list=new Page<Reply>(pageIndex,pageSize);

	        page_list=replyService.page(page_list,wrapper);

	        replyList = page_list.getRecords();//得到集合
	        
	        for(int i=0;i<replyList.size();i++){
	        	if(replyList.get(i).getUid()!=null){
	        		replyList.get(i).setReplyName(userService.getById(replyList.get(i).getUid()).getName());
	        	}else if(replyList.get(i).getRid()!=null){
	        		replyList.get(i).setReplyName(teacherService.getById(replyList.get(i).getRid()).getName());
	        	}
	        
	        }

	        //总数量（表）
	        int totalCount = (int)page_list.getTotal();

	        //总页数
	        PageSupport pages = new PageSupport();
	        pages.setCurrentPageNo(pageIndex);
	        pages.setPageSize(pageSize);
	        pages.setTotalCount(totalCount);

	        int totalPageCount = pages.getTotalPageCount();

	        //控制首页和尾页
	        if (pageIndex < 1) {
	            pageIndex = 1;
	        } else if (pageIndex > totalPageCount) {
	            pageIndex = totalPageCount;
	        }

	        request.setAttribute("replyList", replyList);
	        request.setAttribute("totalPageCount", totalPageCount);
	        request.setAttribute("totalCount", totalCount);
	        request.setAttribute("currentPageNo", pageIndex);
	        return "front/jsp/userReply";
	}
	
		//根据老师的tid查询回复
		@RequestMapping("getTeacherReply")
		public String getTeacherReply(Integer tid,Integer pageIndex,HttpServletRequest request){
			 List<Reply> replyList = null;
		        //设置页面容量
		        int pageSize = Constants.pageSize;
		        if (pageIndex == null) {
		            pageIndex = 1;
		        }
		        
		        QueryWrapper<Reply> wrapper=new QueryWrapper<Reply>();
		        wrapper.eq("isdel", 0);
		        wrapper.in("rid", tid);
		        wrapper.eq("isadmin", 0);
		        wrapper.orderByDesc("id");
	        	
		        //产生page对象，传递当前页码和每页显示多少
		        IPage<Reply> page_list=new Page<Reply>(pageIndex,pageSize);

		        page_list=replyService.page(page_list,wrapper);

		        replyList = page_list.getRecords();//得到集合
		        
		        for(int i=0;i<replyList.size();i++){
		        	Message message=messageService.getById(replyList.get(i).getMid());
		        	replyList.get(i).setUserName(userService.getById(message.getUid()).getName());
		        
		        }

		        //总数量（表）
		        int totalCount = (int)page_list.getTotal();

		        //总页数
		        PageSupport pages = new PageSupport();
		        pages.setCurrentPageNo(pageIndex);
		        pages.setPageSize(pageSize);
		        pages.setTotalCount(totalCount);

		        int totalPageCount = pages.getTotalPageCount();

		        //控制首页和尾页
		        if (pageIndex < 1) {
		            pageIndex = 1;
		        } else if (pageIndex > totalPageCount) {
		            pageIndex = totalPageCount;
		        }

		        request.setAttribute("replyList", replyList);
		        request.setAttribute("totalPageCount", totalPageCount);
		        request.setAttribute("totalCount", totalCount);
		        request.setAttribute("currentPageNo", pageIndex);
		        return "front/jsp/teacherReply";
		}

	//查看回复详情
	@RequestMapping("userGetContent")
	public String userGetContent(Integer id,HttpServletRequest request){
		Reply reply=replyService.getById(id);
		Message message=messageService.getById(reply.getMid());
		
		if(reply.getUid()!=null){
			reply.setUserName(userService.getById(message.getUid()).getName());
    	}else if(reply.getRid()!=null&&reply.getIsadmin()==0){
    		reply.setReplyName(teacherService.getById(reply.getRid()).getName());
    	}else if(reply.getRid()!=null&&reply.getIsadmin()==1){
    		reply.setReplyName(userService.getById(reply.getRid()).getName());
    	}
        
		reply.setUserName(userService.getById(message.getUid()).getName());
		
		if(reply!=null){
			request.setAttribute("reply", reply);
			request.setAttribute("message", message);
			return "front/jsp/userReplyview";
		}else{
			return "redirect:/qjy/reply/getUerReply";
		}
	}
}

