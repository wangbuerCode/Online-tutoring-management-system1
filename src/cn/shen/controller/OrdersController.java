package cn.shen.controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.shen.entity.Need;
import cn.shen.entity.Orders;
import cn.shen.entity.Teacher;
import cn.shen.entity.User;
import cn.shen.service.NeedService;
import cn.shen.service.OrdersService;
import cn.shen.service.TeacherService;
import cn.shen.service.UserService;
import cn.shen.tools.Constants;
import cn.shen.tools.PageSupport;
import cn.shen.tools.ServerResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mysql.jdbc.StringUtils;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Controller
@RequestMapping("/order")
public class OrdersController {
	@Autowired
	OrdersService ordersService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	TeacherService teacherService;
	
	@Autowired
	NeedService needService;
	
	//分页查询
	@RequestMapping("list")
	public String list(String name,Integer isend,Integer pageIndex,HttpServletRequest request){
        List<Orders> orderList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        
        
        //需求查询条件
        QueryWrapper<Orders> wrapper=new QueryWrapper<Orders>();
        if(!StringUtils.isNullOrEmpty(name)){
        	QueryWrapper<User> Uwrapper=new QueryWrapper<User>();
        	Uwrapper.like("name",name);
        	List<User> list=userService.list(Uwrapper);
        	List<Object> uid=new ArrayList<Object>();
        	if(list.size()>0){
        		for(int i=0;i<list.size();i++){
                	uid.add(list.get(i).getId());
                }
            	wrapper.in("uid", uid);
        	}else{
        		wrapper.eq("uid", 0);
        	}
        }
        //是否结束订单
        if(isend!=null&&isend!=-1){
        	wrapper.eq("isend", isend);
        }
        //是否删除
        wrapper.eq("isdel", 0);
        wrapper.orderByDesc("id");
        //产生page对象，传递当前页码和每页显示多少
        IPage<Orders> page_list=new Page<Orders>(pageIndex,pageSize);

        page_list=ordersService.page(page_list,wrapper);

        orderList = page_list.getRecords();//得到集合
        
        for(int i=0;i<orderList.size();i++){
        	orderList.get(i).setUserName(userService.getById(orderList.get(i).getUid()).getName());
        	orderList.get(i).setTeacherName(teacherService.getById(orderList.get(i).getTid()).getName());
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("orderList", orderList);
        request.setAttribute("name", name);
        request.setAttribute("isend", isend);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "manage/jsp/orderlist";
    }
	
	//通过ID查询需求
	@RequestMapping("getById")
	public String getById(String method,String id,HttpServletRequest request){
		 if(method.equals("view")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Orders order = ordersService.getById(id);
	                order.setUserName(userService.getById(order.getUid()).getName());
	                order.setTeacherName(teacherService.getById(order.getTid()).getName());
	                request.setAttribute("order", order);
	                return "manage/jsp/orderview";
	            }else{
	                return "order/list";
	            }
	        }else if(method.equals("modify")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Orders order = ordersService.getById(id);
	                order.setUserName(userService.getById(order.getUid()).getName());
	                order.setTeacherName(teacherService.getById(order.getTid()).getName());
	                request.setAttribute("order", order);
	                return "manage/jsp/ordermodify";
	            }else{
	                return "order/list";
	            }
	        }else if(method.equals("qianview")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Orders order = ordersService.getById(id);
	                order.setUserName(userService.getById(order.getUid()).getName());
	                order.setTeacherName(teacherService.getById(order.getTid()).getName());
	                request.setAttribute("order", order);
	                request.setAttribute("pic", userService.getById(order.getUid()).getPic());
	                return "front/jsp/myorderview";
	            }else{
	                return "order/getByUid";
	            }
	        }else{
	            return "order/list";
	        }
	}

	//需求注册
	@RequestMapping("add")
	@ResponseBody
	public ServerResponse add(Orders order){
		Date today=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String time=sdf.format(today);
		
		order.setTime(time);
		order.setTimes(0);
		order.setIsdel(0);
		order.setIsend(0);
		order.setIsUagree(0);
   	 	order.setIsTagree(0);
		if(ordersService.save(order)){
			return  new ServerResponse("0", "添加成功!");
        }else{
        	return  new ServerResponse("1", "添加失败!");
        }
	}

	//需求修改
	@RequestMapping("modify")
	@ResponseBody
	public ServerResponse modify(Orders order){
		if(order.getIsend()==-1){
			return  new ServerResponse("1", "修改失败!");
		}
		if(order.getTimes()!=null){
			Teacher teacher=teacherService.getById(order.getTid());
			Integer price=teacher.getPrice()*order.getTimes();
			order.setPrice(price);
		}
		
		if(ordersService.updateById(order)){
			return  new ServerResponse("0", "修改成功!");
        }else{
        	return  new ServerResponse("1", "修改失败!");
        }
	}
	
	//需求删除
	@RequestMapping("del")
	public String del(Integer id,HttpServletResponse response){
		Orders order=ordersService.getById(id);
		if(order.getIsend()==1){
			order.setIsdel(1);
			if(ordersService.updateById(order)){
	            return "redirect:list";
	        }else{
	            try {
	                PrintWriter outPrintWriter = response.getWriter();
	                outPrintWriter.println("<script> alert('删除失败！')</script>");
	                outPrintWriter.flush();
	                outPrintWriter.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	            return "redirect:list";
	        }
		}else{
			try {
                PrintWriter outPrintWriter = response.getWriter();
                outPrintWriter.println("<script> alert('订单未完成，无法删除！')</script>");
                outPrintWriter.flush();
                outPrintWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "redirect:list";
		}
	}

	//老师应聘学生发布的需求OK
	@RequestMapping("teacher")
	@ResponseBody
	public ServerResponse teacher(Integer tid,Integer nid,Integer uid){
		 List<Orders> orderList = null;
		//需求查询条件
	     QueryWrapper<Orders> wrapper=new QueryWrapper<Orders>();
	     if(tid!=null&&tid!=0){
	    	 wrapper.eq("tid", tid);
	     }
	     if(uid!=null&&uid!=0){
	    	 wrapper.eq("uid", uid);
	     }
	     if(nid!=null&&nid!=0){
	    	 wrapper.eq("nid", nid);
	     }
	     wrapper.eq("istry",0);
	     wrapper.eq("isdel",0);
	     wrapper.eq("isend",0);
	     wrapper.eq("isevaluation",0);
	     wrapper.eq("level",0);
	     orderList=ordersService.list(wrapper);
	     
	     if(orderList.size()>0){
	    	 return  new ServerResponse("1", "已应聘！");
	     }else{
	    	 //通知用户
	    	 User user=userService.getById(uid);
	    	 if(user.getIsnotice()==0){
	    		 user.setIsnotice(1);
	    	 }
	    	 boolean b=userService.updateById(user);
	    	 
	    	 
	    	 //创建订单
	    	 Orders orders=new Orders();
	    	 Need need=needService.getById(nid);
	    	 Date today=new Date();
	 		 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 		 String time=sdf.format(today);
	 		 
	    	 orders.setUid(uid);
	    	 orders.setTid(tid);
	    	 orders.setNid(nid);
	    	 orders.setGrade(need.getGrade());
	    	 orders.setSubject(need.getSubject());
	    	 orders.setTimes(0);
	    	 orders.setTime(time);
	    	 orders.setPrice(need.getPrice());
	    	 orders.setIsend(0);
	    	 orders.setIstry(0);
	    	 orders.setIsdel(0);
	    	 orders.setIsevaluation(0);
	    	 orders.setLevel(0);
	    	 orders.setIsUagree(0);
	    	 orders.setIsTagree(0);
	    	 orders.setClasstime(need.getStudyhour());
	    	 
	    	 if(ordersService.save(orders)){
	    		 return  new ServerResponse("0", "请等待用户审核！");
	    	 }else{
	    		 return  new ServerResponse("1", "创建订单失败");
	    	 }
	     }
	}
	
	//学生聘用老师OK
	@RequestMapping("user")
	@ResponseBody
	public ServerResponse user(Integer tid,Integer uid){
		 List<Orders> orderList = null;
		//需求查询条件
	     QueryWrapper<Orders> wrapper=new QueryWrapper<Orders>();
	     if(tid!=null&&tid!=0){
	    	 wrapper.eq("tid", tid);
	     }
	     if(uid!=null&&uid!=0){
	    	 wrapper.eq("uid", uid);
	     }
	     wrapper.eq("nid",0);
	     wrapper.eq("isdel",0);
	     wrapper.eq("isend",0);
	     orderList=ordersService.list(wrapper);
	     if(orderList.size()>0){
	    	 return  new ServerResponse("1", "已聘用！");
	     }else{
	    	 //通知老师
	    	 Teacher teacher=teacherService.getById(tid);
	    	 if(teacher.getIsnotice()==0){
	    		 teacher.setIsnotice(1);
	    	 }
	    	 boolean b=teacherService.updateById(teacher);
	    	 
	    	 //创建订单
	    	 Orders orders=new Orders();
	    	 Date today=new Date();
	 		 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 		 String time=sdf.format(today);
	 		 
	 		 orders.setUid(uid);
	    	 orders.setTid(tid);
	    	 orders.setNid(0);
	    	 orders.setGrade(teacher.getGrade());
	    	 orders.setSubject(teacher.getSubject());
	    	 orders.setTimes(0);
	    	 orders.setTime(time);
	    	 orders.setPrice(teacher.getPrice());
	    	 orders.setIsend(0);
	    	 orders.setIstry(0);
	    	 orders.setIsdel(0);
	    	 orders.setIsevaluation(0);
	    	 orders.setLevel(0);
	    	 orders.setIsUagree(0);
	    	 orders.setIsTagree(0);
	    	 orders.setClasstime(teacher.getTime());
	    	 
	    	 if(ordersService.save(orders)){
	    		 return  new ServerResponse("0", "请等待老师同意！");
	    	 }else{
	    		 return  new ServerResponse("1", "创建订单失败");
	    	 }
	     }
	}

	//通过用户uid查询该用户的所有订单OK
	@RequestMapping("getByUid")
	public String getByUid(Integer uid,Integer isend,Integer isevaluation,Integer pageIndex,HttpServletRequest request){
		List<Orders> orderList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        
        
        //需求查询条件
        QueryWrapper<Orders> wrapper=new QueryWrapper<Orders>();
        if(uid!=null&&uid!=0){
        	wrapper.eq("uid", uid);
        }
        //是否结束订单
        if(isend!=null&&isend!=-1){
        	wrapper.eq("isend", isend);
        }
        if(isevaluation!=null&&isevaluation!=-1){
        	wrapper.eq("isevaluation", isevaluation);
        }
        //是否删除
        wrapper.eq("isdel", 0);
        wrapper.orderByDesc("id");
        //产生page对象，传递当前页码和每页显示多少
        IPage<Orders> page_list=new Page<Orders>(pageIndex,pageSize);

        page_list=ordersService.page(page_list,wrapper);

        orderList = page_list.getRecords();//得到集合
        
        for(int i=0;i<orderList.size();i++){
        	orderList.get(i).setUserName(userService.getById(orderList.get(i).getUid()).getName());
        	orderList.get(i).setTeacherName(teacherService.getById(orderList.get(i).getTid()).getName());
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("orderList", orderList);
        request.setAttribute("isend", isend);
        request.setAttribute("isevaluation", isevaluation);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "front/jsp/userOrder";
	}

	//用户同意订单OK
	@RequestMapping("userAgree")
	@ResponseBody
	public ServerResponse userAgree(Integer id,Integer uid){
		Orders orders=null;
		User user=null;
		if(id!=null&&id!=0){
			orders=ordersService.getById(id);
		}
		if(uid!=null&&uid!=0){
			user=userService.getById(uid);
		}
		user.setIsnotice(0);
		boolean b=userService.updateById(user);
		if(orders.getIsUagree()==1&&b){
			return   new ServerResponse("1", "订单已同意！");
		}else{
			orders.setIsUagree(1);;
			if(ordersService.updateById(orders)&&b){
				return  new ServerResponse("0", "订单已同意！");
			}else{
				return  new ServerResponse("1", "同意订单失败！");
			}
		}
	}
	
	//前台用户删除已完成订单
	@RequestMapping("qianDel")
	@ResponseBody
	public ServerResponse qianDel(Integer id){
		Orders orders=ordersService.getById(id);
		if(orders!=null){
			if(orders.getIsend()==1){
				orders.setIsdel(1);
				if(ordersService.updateById(orders)){
					return  new ServerResponse("0", "删除成功！");
				}else{
					return  new ServerResponse("1", "删除失败！");
				}
			}else{
				return  new ServerResponse("1", "订单未完成！");
			}
		}else{
			return  new ServerResponse("1", "订单不存在！");
		}
	}
	
	//用户完成订单ok
	@RequestMapping("complete")
	@ResponseBody
	public ServerResponse complete(Integer id){
		Orders orders=ordersService.getById(id);
		if(orders!=null){
			if(orders.getIsend()==0){
				if(orders.getIsTagree()==1&&orders.getIsUagree()==1){
					if(orders.getTimes()>0){
						orders.setIsend(1);
						if(ordersService.updateById(orders)){
							return  new ServerResponse("0", "完成订单！");
						}else{
							return  new ServerResponse("1", "完成订单失败！");
						}
					}else{
						return  new ServerResponse("1", "试教中或者上课次数为0！");
					}
				}else{
					return  new ServerResponse("1", "订单双方未同意！");
				}
			}else{
				return  new ServerResponse("1", "订单已完成！");
			}
		}else{
			return  new ServerResponse("1", "订单不存在！");
		}
	}
	
	//对订单进行评价ok
	@RequestMapping("evaluation")
	@ResponseBody
	public ServerResponse evaluation(Integer oid,String evaluation,Integer level){
		Orders orders=ordersService.getById(oid);
		if(orders!=null){
			if((orders.getEvaluation()==null||orders.getEvaluation().equals(""))&&orders.getLevel()==0){
				if(evaluation!=null&&level>0&&level<=5){
					Date today=new Date();
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String time=sdf.format(today);
					
					orders.setEvaluationtime(time);
					orders.setEvaluation(evaluation);
					orders.setIsevaluation(1);
					orders.setLevel(level);
					if(ordersService.updateById(orders)){
						return  new ServerResponse("0", "评论订单成功！");
					}else{
						return  new ServerResponse("1", "评论订单失败！");
					}
				}else{
					return  new ServerResponse("1", "评论不能为空！");
				}
			}else{
				return  new ServerResponse("1", "订单已经评论过！");
			}
		}else{
			return  new ServerResponse("1", "订单不存在！");
		}
	}

	//修改订单评价
	@RequestMapping("updateEvaluation")
	@ResponseBody
	public ServerResponse updateEvaluation(Integer oid,String evaluation){
		Orders orders=ordersService.getById(oid);
		if(orders!=null){
			if(orders.getEvaluation()!=null&&orders.getLevel()!=null&&!(evaluation.equals(""))){
					Date today=new Date();
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String time=sdf.format(today);
					
					orders.setEvaluationtime(time);
					orders.setEvaluation(evaluation);
					orders.setIsevaluation(1);
					if(ordersService.updateById(orders)){
						return  new ServerResponse("0", "修改评论成功！");
					}else{
						return  new ServerResponse("1", "修改评论失败！");
					}
			}else{
				return  new ServerResponse("1", "没有评论无法修改！");
			}
		}else{
			return  new ServerResponse("1", "订单不存在！");
		}
	}

//	//删除订单评价
//		@RequestMapping("delEvaluation")
//		@ResponseBody
//	public ServerResponse delEvaluation(Integer id){
//			Orders orders=ordersService.getById(id);
//			if(orders!=null){
//				if(orders.getEvaluation()!=null){
//					orders.setEvaluationtime("");
//					orders.setEvaluation("");
//					orders.setIsevaluation(0);
//					if(ordersService.updateById(orders)){
//						return  new ServerResponse("0", "删除评论成功！");
//					}else{
//						return  new ServerResponse("1", "删除评论失败！");
//					}
//				}else{
//					return  new ServerResponse("1", "没有评论无法删除！");
//				}
//			}else{
//				return  new ServerResponse("1", "订单不存在！");
//			}
//		}

	//账单查询，通过用户uid查询该用户的所有已经完成的订单
	@RequestMapping("getBill")
	public String getBill(Integer uid,String start,String end,Integer pageIndex,HttpServletRequest request){
		List<Orders> orderList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        
        
        //需求查询条件
        QueryWrapper<Orders> wrapper=new QueryWrapper<Orders>();
        if(uid!=null&&uid!=0){
        	wrapper.eq("uid", uid);
        }
        //是否结束订单
        if(!StringUtils.isNullOrEmpty(start)&&StringUtils.isNullOrEmpty(end)){
        	wrapper.ge("time", start);
        }else if(StringUtils.isNullOrEmpty(start)&&!StringUtils.isNullOrEmpty(end)){
        	wrapper.le("time", end);
        }else if(!StringUtils.isNullOrEmpty(start)&&!StringUtils.isNullOrEmpty(end)){
        	wrapper.between("time", start, end);
        }
        //是否删除
        wrapper.eq("isdel", 0);
        wrapper.eq("isend", 1);
        wrapper.ge("times", 1);
        wrapper.orderByDesc("id");
        //产生page对象，传递当前页码和每页显示多少
        IPage<Orders> page_list=new Page<Orders>(pageIndex,pageSize);

        page_list=ordersService.page(page_list,wrapper);

        orderList = page_list.getRecords();//得到集合
        
        Integer sum=0;
        for(int i=0;i<orderList.size();i++){
        	orderList.get(i).setUserName(userService.getById(orderList.get(i).getUid()).getName());
        	orderList.get(i).setTeacherName(teacherService.getById(orderList.get(i).getTid()).getName());
        	sum+=orderList.get(i).getPrice();
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("orderList", orderList);
        request.setAttribute("start", start);
        request.setAttribute("end", end);
        request.setAttribute("sum", sum);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "front/jsp/userBill";
	}

	//通过老师tid查询该用户的所有订单
	@RequestMapping("getByTid")
	public String getByTid(Integer tid,Integer isend,Integer isevaluation,Integer pageIndex,HttpServletRequest request){
			List<Orders> orderList = null;
	        //设置页面容量
	        int pageSize = Constants.pageSize;
	        if (pageIndex == null) {
	            pageIndex = 1;
	        }
	        
	        
	        //需求查询条件
	        QueryWrapper<Orders> wrapper=new QueryWrapper<Orders>();
	        if(tid!=null&&tid!=0){
	        	wrapper.eq("tid", tid);
	        }
	        //是否结束订单
	        if(isend!=null&&isend!=-1){
	        	wrapper.eq("isend", isend);
	        }
	        if(isevaluation!=null&&isevaluation!=-1){
	        	wrapper.eq("isevaluation", isevaluation);
	        }
	        //是否删除
	        wrapper.eq("isdel", 0);
	        wrapper.orderByDesc("id");
	        //产生page对象，传递当前页码和每页显示多少
	        IPage<Orders> page_list=new Page<Orders>(pageIndex,pageSize);

	        page_list=ordersService.page(page_list,wrapper);

	        orderList = page_list.getRecords();//得到集合
	        
	        for(int i=0;i<orderList.size();i++){
	        	orderList.get(i).setUserName(userService.getById(orderList.get(i).getUid()).getName());
	        	orderList.get(i).setTeacherName(teacherService.getById(orderList.get(i).getTid()).getName());
	        }

	        //总数量（表）
	        int totalCount = (int)page_list.getTotal();

	        //总页数
	        PageSupport pages = new PageSupport();
	        pages.setCurrentPageNo(pageIndex);
	        pages.setPageSize(pageSize);
	        pages.setTotalCount(totalCount);

	        int totalPageCount = pages.getTotalPageCount();

	        //控制首页和尾页
	        if (pageIndex < 1) {
	            pageIndex = 1;
	        } else if (pageIndex > totalPageCount) {
	            pageIndex = totalPageCount;
	        }

	        request.setAttribute("orderList", orderList);
	        request.setAttribute("isend", isend);
	        request.setAttribute("isevaluation", isevaluation);
	        request.setAttribute("totalPageCount", totalPageCount);
	        request.setAttribute("totalCount", totalCount);
	        request.setAttribute("currentPageNo", pageIndex);
	        return "front/jsp/teacherOrder";
		}

	//修改订单次数OK
	@RequestMapping("times")
	@ResponseBody
	public ServerResponse times(Integer oid,Integer times){
		Orders orders=ordersService.getById(oid);
		if(orders!=null){
			if(orders.getNid()==0){
				if(times!=null&&times!=0){
					Teacher teacher=teacherService.getById(orders.getTid());
					Integer price=teacher.getPrice()*times;
					orders.setPrice(price);
					orders.setTimes(times);
					orders.setIstry(1);
					
					if(ordersService.updateById(orders)){
						return  new ServerResponse("0", "修改成功！");
					}else{
						return  new ServerResponse("1", "修改失败！");
					}
				}else{
					return  new ServerResponse("1", "上课次数不能为空或0！");
				}
			}else{
				if(times!=null&&times!=0){
					Need need=needService.getById(orders.getNid());
					Integer price=need.getPrice()*times;
					orders.setPrice(price);
					orders.setTimes(times);
					
					if(ordersService.updateById(orders)){
						return  new ServerResponse("0", "修改成功！");
					}else{
						return  new ServerResponse("1", "修改失败！");
					}
				}else{
					return  new ServerResponse("1", "上课次数不能为空或0！");
				}
			}
		}else{
			return  new ServerResponse("1", "订单不存在！");
		}
	}

	//老师同意订单
		@RequestMapping("teacherAgree")
		@ResponseBody
	public ServerResponse teacherAgree(Integer id,Integer tid){
			Orders orders=null;
			Teacher teacher=null;
			if(id!=null&&id!=0){
				orders=ordersService.getById(id);
			}
			if(tid!=null&&tid!=0){
				teacher=teacherService.getById(tid);
			}
			teacher.setIsnotice(0);
			boolean b=teacherService.updateById(teacher);
			if(orders.getIsTagree()==1&&b){
				return   new ServerResponse("1", "订单已同意！");
			}else{
				orders.setIsTagree(1);;
				if(ordersService.updateById(orders)&&b){
					return  new ServerResponse("0", "订单已同意！");
				}else{
					return  new ServerResponse("1", "同意订单失败！");
				}
			}
		}
	
	//前台首页查询最新五星好评订单列表
	@RequestMapping("indexLevelList")
	@ResponseBody
	public List<Orders> indexLevelList() {
		List<Orders> list = null;

		// 条件
		QueryWrapper<Orders> wrapper = new QueryWrapper<Orders>();
		wrapper.eq("level", 5);
		wrapper.eq("isdel", 0);
		wrapper.eq("isevaluation", 1);
		wrapper.orderByDesc("id");

		IPage<Orders> page_list = new Page<Orders>(0, 4);
		page_list = ordersService.page(page_list, wrapper);

		list = page_list.getRecords();// 得到集合
		if(list.size()>0){
			for (int i = 0; i < list.size(); i++) {
				list.get(i).setUserName(userService.getById(list.get(i).getUid()).getName());
				list.get(i).setTeacherName(teacherService.getById(list.get(i).getTid()).getName());
			}
		}
		return list;
	}
	
	// 前台首页查询最新完成订单列表
	@RequestMapping("indexNewList")
	@ResponseBody
	public List<Orders> indexNewList() {
		List<Orders> list = null;

		// 条件
		QueryWrapper<Orders> wrapper = new QueryWrapper<Orders>();
		wrapper.eq("isdel", 0);
		wrapper.eq("isUagree", 1);
		wrapper.eq("isTagree", 1);
		wrapper.orderByDesc("id");

		IPage<Orders> page_list = new Page<Orders>(0, 4);
		page_list = ordersService.page(page_list, wrapper);

		list = page_list.getRecords();// 得到集合
		if(list.size()>0){
			for (int i = 0; i < list.size(); i++) {
				list.get(i).setUserName(userService.getById(list.get(i).getUid()).getName());
				list.get(i).setTeacherName(teacherService.getById(list.get(i).getTid()).getName());
			}
		}
		return list;
	}
	
}


