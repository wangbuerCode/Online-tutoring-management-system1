package cn.shen.controller;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.shen.entity.Attention;
import cn.shen.service.AttentionService;
import cn.shen.service.TeacherService;
import cn.shen.service.UserService;
import cn.shen.tools.Constants;
import cn.shen.tools.PageSupport;
import cn.shen.tools.ServerResponse;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Controller
@RequestMapping("/attention")
public class AttentionController {
	@Autowired
	AttentionService attentionService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	TeacherService teacherService;
	
	//用户查询关注的老师
	@RequestMapping("teacherList")
	public String teacherList(Integer uid,Integer pageIndex,HttpServletRequest request){
		List<Attention> attentionList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        
        
        //需求查询条件
        QueryWrapper<Attention> wrapper=new QueryWrapper<Attention>();
        if(uid!=null&&uid!=0){
        	wrapper.eq("uid", uid);
        }
        //是否删除
        wrapper.eq("isdel", 0);
        wrapper.eq("status", 1);
        //产生page对象，传递当前页码和每页显示多少
        IPage<Attention> page_list=new Page<Attention>(pageIndex,pageSize);

        page_list=attentionService.page(page_list,wrapper);

        attentionList = page_list.getRecords();//得到集合
        
        for(int i=0;i<attentionList.size();i++){
//        	teacherService.getById(attentionList.get(i).getBtid()).getId();
        	attentionList.get(i).setGrade(teacherService.getById(attentionList.get(i).getBtid()).getGrade());
        	attentionList.get(i).setSubject(teacherService.getById(attentionList.get(i).getBtid()).getSubject());
        	attentionList.get(i).setTeacherName(teacherService.getById(attentionList.get(i).getBtid()).getName());
        }

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("attentionList", attentionList);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "front/jsp/userAttention";
	}
	
	//老师查询关注的学生
	@RequestMapping("userList")
	public String userList(Integer tid,Integer pageIndex,HttpServletRequest request){
			List<Attention> attentionList = null;
	        //设置页面容量
	        int pageSize = Constants.pageSize;
	        if (pageIndex == null) {
	            pageIndex = 1;
	        }
	        
	        
	        //需求查询条件
	        QueryWrapper<Attention> wrapper=new QueryWrapper<Attention>();
	        if(tid!=null&&tid!=0){
	        	wrapper.eq("tid", tid);
	        }
	        //是否删除
	        wrapper.eq("isdel", 0);
	        wrapper.eq("status", 1);
	        //产生page对象，传递当前页码和每页显示多少
	        IPage<Attention> page_list=new Page<Attention>(pageIndex,pageSize);

	        page_list=attentionService.page(page_list,wrapper);

	        attentionList = page_list.getRecords();//得到集合
	        
	        for(int i=0;i<attentionList.size();i++){
	        	attentionList.get(i).setGrade(userService.getById(attentionList.get(i).getBuid()).getGrade());
	        	attentionList.get(i).setSubject(userService.getById(attentionList.get(i).getBuid()).getSubject());
	        	attentionList.get(i).setUserName(userService.getById(attentionList.get(i).getBuid()).getName());
	        }

	        //总数量（表）
	        int totalCount = (int)page_list.getTotal();

	        //总页数
	        PageSupport pages = new PageSupport();
	        pages.setCurrentPageNo(pageIndex);
	        pages.setPageSize(pageSize);
	        pages.setTotalCount(totalCount);

	        int totalPageCount = pages.getTotalPageCount();

	        //控制首页和尾页
	        if (pageIndex < 1) {
	            pageIndex = 1;
	        } else if (pageIndex > totalPageCount) {
	            pageIndex = totalPageCount;
	        }

	        request.setAttribute("attentionList", attentionList);
	        request.setAttribute("totalPageCount", totalPageCount);
	        request.setAttribute("totalCount", totalCount);
	        request.setAttribute("currentPageNo", pageIndex);
	        return "front/jsp/teacherAttention";
		}
	
	//老师关注学生
	@RequestMapping("teacher")
	@ResponseBody
	public ServerResponse teacher(Integer tid,Integer buid){
		List<Attention> list=null;
		QueryWrapper<Attention> wrapper=new QueryWrapper<Attention>();
		//条件
		if(tid!=null&&tid!=0){
	    	 wrapper.eq("tid", tid);
	     }
	     if(buid!=null&&buid!=0){
	    	 wrapper.eq("buid", buid);
	     }
	     wrapper.eq("isdel",0);
	     wrapper.eq("status", 1);
	     list=attentionService.list(wrapper);
	     if(list.size()>0){
	    	 for(int i=0;i<list.size();i++){
	    		 if(list.get(i).getStatus()==1){
	    			 return new ServerResponse("1", "已关注学生！");
	    		 }else{
	    			 list.get(i).setStatus(1);
	    			 return new ServerResponse("0", "关注成功！");
	    		 }
	    	 }
	     }else{
	    	 Attention attention=new Attention();
	    	 Date today=new Date();
	 		 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	 		 String time=sdf.format(today);
	 		 
	    	 attention.setTid(tid);
	    	 attention.setBuid(buid);
	    	 attention.setStatus(1);
	    	 attention.setTime(time);
	    	 attention.setIsdel(0);
	    	 
	    	 if(attentionService.save(attention)){
	    		 return new ServerResponse("0", "关注成功！");
	    	 }else{
	    		 return new ServerResponse("1", "关注失败！");
	    	 }
	     }
	     return new ServerResponse("1", "关注失败！");
	}
	
	//学生关注老师
		@RequestMapping("user")
		@ResponseBody
	public ServerResponse user(Integer uid,Integer btid){
			List<Attention> list=null;
			QueryWrapper<Attention> wrapper=new QueryWrapper<Attention>();
			//条件
			if(uid!=null&&uid!=0){
		    	 wrapper.eq("uid", uid);
		     }
		     if(btid!=null&&btid!=0){
		    	 wrapper.eq("btid", btid);
		     }
		     wrapper.eq("isdel",0);
		     wrapper.eq("status", 1);
		     list=attentionService.list(wrapper);
		     if(list.size()>0){
		    	 for(int i=0;i<list.size();i++){
		    		 if(list.get(i).getStatus()==1){
		    			 return new ServerResponse("1", "已关注该老师！");
		    		 }else{
		    			 list.get(i).setStatus(1);
		    			 return new ServerResponse("0", "关注成功！");
		    		 }
		    	 }
		     }else{
		    	 Attention attention=new Attention();
		    	 Date today=new Date();
		 		 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		 		 String time=sdf.format(today);
		 		 
		    	 attention.setUid(uid);
		    	 attention.setBtid(btid);
		    	 attention.setStatus(1);
		    	 attention.setTime(time);
		    	 attention.setIsdel(0);
		    	 
		    	 if(attentionService.save(attention)){
		    		 return new ServerResponse("0", "关注成功！");
		    	 }else{
		    		 return new ServerResponse("1", "关注失败！");
		    	 }
		     }
		     return new ServerResponse("1", "关注失败！");
		}
		
		
	//老师取消关注
	@RequestMapping("tcancel")
	@ResponseBody
	public ServerResponse tcancel(Integer id){
		Attention attention=attentionService.getById(id);
		if(attention!=null){
			if(attention.getStatus()==1&&attention.getTid()!=null&&attention.getBuid()!=null){
				attention.setStatus(0);
				if(attentionService.updateById(attention)){
					return new ServerResponse("0", "取消关注成功！");
				}else{
					return new ServerResponse("1", "取消关注失败！");
				}
			}else{
				return new ServerResponse("1", "未关注或者该数据不存在！");
			}
		}else{
			return new ServerResponse("1", "未关注！");
		}
	}
	
	//学生取消关注
	@RequestMapping("ucancel")
	@ResponseBody
	public ServerResponse ucancel(Integer id){
		Attention attention=attentionService.getById(id);
		if(attention!=null){
			if(attention.getStatus()==1&&attention.getUid()!=null&&attention.getBtid()!=null){
				attention.setStatus(0);
				if(attentionService.updateById(attention)){
					return new ServerResponse("0", "取消关注成功！");
				}else{
					return new ServerResponse("1", "取消关注失败！");
				}
			}else{
				return new ServerResponse("1", "未关注或者该数据不存在！");
			}
		}else{
			return new ServerResponse("1", "未关注！");
		}
	}
			
}

