package cn.shen.controller;


import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Controller
public class UploadController {
	@RequestMapping("/upfile")
    @ResponseBody
    public JSONObject upfile(MultipartFile file, HttpServletRequest request) {
        JSONObject result=new JSONObject();
        //获取服务器的路径
        String path = request.getSession().getServletContext().getRealPath("/img");
        String filename = "";
        if (file != null) {
            filename = file.getOriginalFilename();
            //传到服务器
            try {
                file.transferTo(new File(path + "/" + filename));
                JSONArray data=new JSONArray();
                JSONObject obj=new JSONObject();
                obj.put("url","/qjy/img/"+filename);
                data.add(obj);
                result.put("errno",0);
                result.put("data",data);
                result.put("filename",filename);
            } catch (IOException e) {
                e.printStackTrace();
                result.put("errno",1);
            }
        }
        return result;
    }

}

