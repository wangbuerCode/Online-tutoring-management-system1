package cn.shen.controller;



import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.shen.entity.Teacher;
import cn.shen.service.TeacherService;
import cn.shen.tools.Constants;
import cn.shen.tools.PageSupport;
import cn.shen.tools.ServerResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mysql.jdbc.StringUtils;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Controller
@RequestMapping("/teacher")
public class TeacherController {
	@Autowired
	TeacherService teacherService;
	
	//分页查询
	@RequestMapping("list")
	public String list(String name,String grade,String subject,Integer pageIndex,HttpServletRequest request){
        List<Teacher> teacherList = null;
        //设置页面容量
        int pageSize = Constants.pageSize;
        if (pageIndex == null) {
            pageIndex = 1;
        }
        //条件
        QueryWrapper<Teacher> wrapper=new QueryWrapper<Teacher>();
        if(!StringUtils.isNullOrEmpty(name)){
            wrapper.like("name",name);
        }
        if(!StringUtils.isNullOrEmpty(grade)){
            wrapper.like("grade",grade);
        }
        if(!StringUtils.isNullOrEmpty(subject)){
            wrapper.like("subject",subject);
        }
        wrapper.eq("role",0);
        wrapper.eq("isdel",0);
        wrapper.orderByDesc("id");

        //产生page对象，传递当前页码和每页显示多少
        IPage<Teacher> page_list=new Page<Teacher>(pageIndex,pageSize);

        page_list=teacherService.page(page_list,wrapper);

        teacherList = page_list.getRecords();//得到集合

        //总数量（表）
        int totalCount = (int)page_list.getTotal();

        //总页数
        PageSupport pages = new PageSupport();
        pages.setCurrentPageNo(pageIndex);
        pages.setPageSize(pageSize);
        pages.setTotalCount(totalCount);

        int totalPageCount = pages.getTotalPageCount();

        //控制首页和尾页
        if (pageIndex < 1) {
            pageIndex = 1;
        } else if (pageIndex > totalPageCount) {
            pageIndex = totalPageCount;
        }

        request.setAttribute("teacherList", teacherList);
        request.setAttribute("name", name);
        request.setAttribute("grade", grade);
        request.setAttribute("subject", subject);
        request.setAttribute("totalPageCount", totalPageCount);
        request.setAttribute("totalCount", totalCount);
        request.setAttribute("currentPageNo", pageIndex);
        return "manage/jsp/teacherlist";
    }
	
	
	//通过ID查询老师	
	@RequestMapping("getById")
	public String getById(String method,String id,HttpServletRequest request){
		 if(method.equals("view")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Teacher teacher = teacherService.getById(id);
	                request.setAttribute("teacher", teacher);
	                return "manage/jsp/teacherview";
	            }else{
	                return "teacher/list";
	            }
	        }else if(method.equals("modify")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Teacher teacher = teacherService.getById(id);
	                request.setAttribute("teacher", teacher);
	                return "manage/jsp/teachermodify";
	            }else{
	                return "teacher/list";
	            }
	        }else if(method.equals("uppwd")){
	            if(!StringUtils.isNullOrEmpty(id)){
	                Teacher teacher = teacherService.getById(id);
	                request.setAttribute("teacher", teacher);
	                return "uppwd";
	            }else{
	                return "redirect:/index.jsp";
	            }
	        }else if(method.equals("qian")){
	        	if(!StringUtils.isNullOrEmpty(id)){
	                Teacher teacher = teacherService.getById(id);
	                request.setAttribute("teacher", teacher);
	                return "front/jsp/personal";
	            }else{
	                return "front/index.jsp";
	            }
			}else if(method.equals("qupdate")){
	        	if(!StringUtils.isNullOrEmpty(id)){
	                Teacher teacher = teacherService.getById(id);
	                request.setAttribute("teacher", teacher);
	                return "front/modify";
	            }else{
	                return "front/index.jsp";
	            }
			}else if(method.equals("quppwd")){
	        	if(!StringUtils.isNullOrEmpty(id)){
	                Teacher teacher = teacherService.getById(id);
	                request.setAttribute("teacher", teacher);
	                return "front/uppwd";
	            }else{
	                return "front/index";
	            }
			}else if(method.equals("details")){
	        	if(!StringUtils.isNullOrEmpty(id)){
	                Teacher teacher = teacherService.getById(id);
	                request.setAttribute("teachers", teacher);
	                return "front/jsp/teacherdetails";
	            }else{
	                return "front/index";
	            }
			}else{
	            return "teacher/list";
	        }
	}

	//老师注册
	@RequestMapping("add")
	@ResponseBody
	public ServerResponse add(Teacher teacher,String repassword){
		if (repassword!=null) {
			if (repassword.equals(teacher.getPassWord())) {
				
				teacher.setRole(0);
				teacher.setIsgood(0);
				teacher.setIsdel(0);
				teacher.setIsnew(1);
				teacher.setIsnotice(0);
				teacher.setIsxinxi(0);
				if(teacherService.save(teacher)){
					return  new ServerResponse("0", "注册成功!");
		        }else{
		        	return  new ServerResponse("1", "添加失败!");
		        }
			}else{
				return  new ServerResponse("1", "输入密码不相符!");
			}
		}else{
			return  new ServerResponse("1", "请再输入一次密码!");
		}
	}

	//老师修改
	@RequestMapping("modify")
	@ResponseBody
	public ServerResponse modify(Teacher teacher){
		if(teacher.getPrice()>0&&teacher.getPrice()<=200){
			teacher.setPrice_jibie(1);
		}else if(teacher.getPrice()>200&&teacher.getPrice()<=500){
			teacher.setPrice_jibie(2);
		}else if(teacher.getPrice()>500&&teacher.getPrice()<=800){
			teacher.setPrice_jibie(3);
		}else if(teacher.getPrice()>800){
			teacher.setPrice_jibie(4);
		}
		teacher.setIsxinxi(1);
		if(teacher.getIsnew()!=-1&&teacher.getIsgood()!=-1){
			if(teacher.getPic().equals("")){
				teacher.setPic(null);
			}
			
			if(teacherService.updateById(teacher)){
				return  new ServerResponse("0", "修改成功!");
	        }else{
	        	return  new ServerResponse("1", "修改失败!");
	        }
		}else{
			return  new ServerResponse("1", "请选择身份或者是否优秀教师!");
		}
	}
	
	//老师个人资料修改
	@RequestMapping("modifyQian")
	@ResponseBody
	public ServerResponse modifyQian(Teacher teacher) {
		if (teacher.getSubject() != null && teacher.getGrade() != null
				&& teacher.getTime() != null && teacher.getPrice() != null) {
			if (teacher.getPrice() > 0 && teacher.getPrice() <= 200) {
				teacher.setPrice_jibie(1);
			} else if (teacher.getPrice() > 200 && teacher.getPrice() <= 500) {
				teacher.setPrice_jibie(2);
			} else if (teacher.getPrice() > 500 && teacher.getPrice() <= 800) {
				teacher.setPrice_jibie(3);
			} else if (teacher.getPrice() > 800) {
				teacher.setPrice_jibie(4);
			}
			
			teacher.setIsxinxi(1);
			
			if (teacherService.updateById(teacher)) {
				return new ServerResponse("0", "修改成功!");
			} else {
				return new ServerResponse("1", "修改失败!");
			}
		}else{
			return new ServerResponse("1", "年级、学科、上课时间、价格不能为空!");
		}
	}
	
	//老师删除
	@RequestMapping("del")
	@ResponseBody
	public ServerResponse del(Integer id){
		Teacher teacher=teacherService.getById(id);
		if(teacher!=null){
			teacher.setIsdel(1);
			if(teacherService.updateById(teacher)){
				return  new ServerResponse("0", "删除成功!");
			}else{
				return  new ServerResponse("1", "删除失败!");
			}
		}else{
			return  new ServerResponse("1", "该用户不存在!");
		}
	}

	//前台首页优秀老师查询
	@RequestMapping("indexList")
	@ResponseBody
	public List<Teacher> indexList(){
		List<Teacher> teacherList = null;
		
		//条件
        QueryWrapper<Teacher> wrapper=new QueryWrapper<Teacher>();
        wrapper.eq("isgood", 1);
        wrapper.eq("isdel", 0);
        wrapper.eq("isxinxi", 1);
        wrapper.orderByDesc("id");
        
        IPage<Teacher> page_list=new Page<Teacher>(0,4);
        page_list=teacherService.page(page_list,wrapper);

        teacherList = page_list.getRecords();//得到集合
        return teacherList;
	}

	//前台首页新老师查询
	@RequestMapping("indexNewList")
	@ResponseBody
	public List<Teacher> indexNewList(){
		List<Teacher> teacherList = null;

		// 条件
		QueryWrapper<Teacher> wrapper = new QueryWrapper<Teacher>();
		wrapper.eq("isnew", 1);
		wrapper.eq("isdel", 0);
		wrapper.eq("isxinxi", 1);
		wrapper.orderByDesc("id");

		IPage<Teacher> page_list = new Page<Teacher>(0, 3);
		page_list = teacherService.page(page_list, wrapper);

		teacherList = page_list.getRecords();// 得到集合
		return teacherList;
	}
	
	
	//用户查询老师
	@RequestMapping("qianList")
	public String qianList(String grade,String subject,Integer price_jibie,Integer pageIndex,HttpServletRequest request){
		 List<Teacher> teacherList = null;
	        //设置页面容量
	        int pageSize = Constants.pageSize;
	        if (pageIndex == null) {
	            pageIndex = 1;
	        }
	        
	        //条件
	        QueryWrapper<Teacher> wrapper=new QueryWrapper<Teacher>();
	        if(!StringUtils.isNullOrEmpty(grade)){
	            wrapper.like("grade",grade);
	        }
	        if(!StringUtils.isNullOrEmpty(subject)){
	            wrapper.like("subject",subject);
	        }
	        if(price_jibie!=null&&price_jibie!=0){
	        	wrapper.eq("price_jibie",price_jibie);
	        }
	        wrapper.eq("isxinxi",1);
	        wrapper.orderByDesc("id");
	        //产生page对象，传递当前页码和每页显示多少
	        IPage<Teacher> page_list=new Page<Teacher>(pageIndex,pageSize);

	        page_list=teacherService.page(page_list,wrapper);

	        teacherList = page_list.getRecords();//得到集合

	        //总数量（表）
	        int totalCount = (int)page_list.getTotal();

	        //总页数
	        PageSupport pages = new PageSupport();
	        pages.setCurrentPageNo(pageIndex);
	        pages.setPageSize(pageSize);
	        pages.setTotalCount(totalCount);

	        int totalPageCount = pages.getTotalPageCount();

	        //控制首页和尾页
	        if (pageIndex < 1) {
	            pageIndex = 1;
	        } else if (pageIndex > totalPageCount) {
	            pageIndex = totalPageCount;
	        }

	        request.setAttribute("teacherList", teacherList);
	        request.setAttribute("price_jibie", price_jibie);
	        request.setAttribute("grade", grade);
	        request.setAttribute("subject", subject);
	        request.setAttribute("totalPageCount", totalPageCount);
	        request.setAttribute("totalCount", totalCount);
	        request.setAttribute("currentPageNo", pageIndex);
	        return "front/jsp/user";
	}

	//通知弹出
	@RequestMapping("teacherNotice")
	@ResponseBody
	public ServerResponse teacherNotice(Integer id) {
		Teacher teacher = teacherService.getById(id);
		if (teacher != null) {
			if (teacher.getIsnotice() == 1) {
				return new ServerResponse("0", "您有新的订单，请前往我的订单查看!");
			} else {
				return new ServerResponse("1", "无消息!");
			}
		} else {
			return new ServerResponse("1", "无消息!");
		}
	}


}

