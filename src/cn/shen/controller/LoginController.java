package cn.shen.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import cn.shen.entity.Teacher;
import cn.shen.entity.User;
import cn.shen.service.TeacherService;
import cn.shen.service.UserService;
import cn.shen.tools.ServerResponse;

/**
 * <p>
 * 前端控制器
 * </p>
 */
@Controller
/**
 * 
 *@RequestMapping处理请求地址映射的注解
 */
@RequestMapping("/login")
public class LoginController {
	@Autowired
	UserService userService;

	@Autowired
	TeacherService teacherService;

	@RequestMapping("qian")
	@ResponseBody
	public ServerResponse qian(String userName, String passWord, Integer role, HttpServletRequest request) {
		if (role == 1) {
			User user = null;
			QueryWrapper<User> wrapper = new QueryWrapper<>();
			wrapper.eq("userName", userName);
			wrapper.eq("passWord", passWord);
			wrapper.eq("role", 1);
			List<User> list = userService.list(wrapper);
			if (list != null && list.size() > 0) {
				user = list.get(0);
			}
			if (user != null) {
				request.getSession().setAttribute("user", user);
				return new ServerResponse("0", "登陆成功!");
			} else {
				request.setAttribute("error", "用户名或密码不正确");
				return new ServerResponse("1", "用户名密码错误!");
			}
		} else if (role == 2) {
			Teacher teacher = null;
			QueryWrapper<Teacher> wrapper = new QueryWrapper<>();
			wrapper.eq("userName", userName);
			wrapper.eq("passWord", passWord);
			List<Teacher> list = teacherService.list(wrapper);
			if (list != null && list.size() > 0) {
				teacher = list.get(0);
			}
			if (teacher != null) {
				request.getSession().setAttribute("teacher", teacher);
				return new ServerResponse("0", "登陆成功!");
			} else {
				request.setAttribute("error", "用户名或密码不正确");
				return new ServerResponse("1", "用户名密码错误!");
			}
		} else {
			return new ServerResponse("1", "请选择身份登录!");
		}
	}
	@RequestMapping("hou")
	@ResponseBody
	public ServerResponse hou(String userName,String passWord,HttpServletRequest request){
		 User user=null;
		 System.out.println("login ============ " );
	     System.out.println("userName ============ "+userName);
	     System.out.println("passWord ============ "+passWord);
	     QueryWrapper<User> wrapper=new QueryWrapper<>();
	     wrapper.eq("userName", userName);
	     wrapper.eq("passWord", passWord);
	     wrapper.eq("role", 0);
	     List<User> list=userService.list(wrapper);
	     if(list!=null&&list.size()>0){
	            user=list.get(0);
	     }
	     if(user!=null){
	    	 request.getSession().setAttribute("admin", user);
	    	 return  new ServerResponse("0", "登陆成功!");
	     }else{
	         request.setAttribute("error", "用户名或密码不正确");
	         return  new ServerResponse("1", "用户名密码错误!");
	     }
	}
	
	//退出
    @RequestMapping("houout")
    public String houout(HttpSession session){
        if(session!=null){
            session.invalidate();
            return "redirect:/manage/login.jsp";
        }else{
            return "redirect:/manage/login.jsp";
        }
    }
	// 退出
	@RequestMapping("qianout")
	public String qianout(HttpSession session) {
		if (session != null) {
			session.invalidate();
			return "redirect:/front/index.jsp";
		} else {
			return "redirect:/front/index.jsp";
		}
	}
}
