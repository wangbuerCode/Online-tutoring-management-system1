package cn.shen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.shen.entity.Teacher;
import cn.shen.entity.User;
import cn.shen.service.TeacherService;
import cn.shen.service.UserService;
import cn.shen.tools.ServerResponse;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@Controller
@RequestMapping("/add") 
public class AddController {
	@Autowired
	UserService userService;
	
	@Autowired
	TeacherService teacherService;
	
	
	/**
	 * 用户注册
	 * 	通过@RequestMapping来指定控制器可以处理哪些URL请求。
	 *  @ResponseBody的作用其实是将java对象转为json格式的数据
	 *  在使用此注解之后不会再走视图处理器，而是直接将数据写入到输入流中，他的效果等同于通过response对象输出指定格式的数据。
	 */

	@RequestMapping("register")
	@ResponseBody
	public ServerResponse register(String userName,String passWord,String rePassWord,String name,String email,String phone,Integer role){
		if(role==1){
			if(passWord.equals(rePassWord)){
				User user=new User();
				user.setUserName(userName);
				user.setPassWord(passWord);
				user.setName(name);
				user.setEmail(email);
				user.setPhone(phone);
				user.setRole(1);
				user.setIsnotice(0);
				user.setIsdel(0);
				if(userService.save(user)){
					return  new ServerResponse("0", "注册成功!");
				}else{
					return  new ServerResponse("1", "注册失败!");
				}
			}else{
				return  new ServerResponse("1", "密码输入不一样!");
			}
        }else if(role==2){
        	if(passWord.equals(rePassWord)){
				Teacher teacher=new Teacher();
				teacher.setUserName(userName);
				teacher.setPassWord(passWord);
				teacher.setName(name);
				teacher.setEmail(email);
				teacher.setPhone(phone);
				teacher.setRole(0);
				teacher.setIsgood(0);
				teacher.setIsdel(0);
				teacher.setIsnew(1);
				teacher.setIsnotice(0);
				teacher.setIsxinxi(0);
				
				if(teacherService.save(teacher)){
					return  new ServerResponse("0", "注册成功!");
				}else{
					return  new ServerResponse("1", "注册失败!");
				}
			}else{
				return  new ServerResponse("1", "密码输入不一样!");
			}
        }else{
        	return  new ServerResponse("1", "请选择身份!");
        }
	}

	
}

