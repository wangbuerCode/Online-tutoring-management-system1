package cn.shen.tools;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import cn.shen.entity.User;
@WebFilter("/manage/*")
public class LoginFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute("admin");
		if (user == null) {
			response.setContentType("text/html;charset=UTF-8");
			request.setCharacterEncoding("UTF-8");
			String uri = req.getRequestURI();
			if (uri.contains("login")||uri.contains("image")||uri.contains(".css")||uri.contains("jquery-2.1.1.js")
					||uri.contains("amazeui.min.js")||uri.contains("login.js")||uri.contains("fonts")
					||uri.contains("app.js")||uri.contains("font-awesome.css")||uri.contains("custom-styles.css")) {
				filterChain.doFilter(request, response);
			} else {	
				PrintWriter out = response.getWriter();
				out.print("<script>alert('请先登录');location.href='/qjy/manage/login.jsp'</script>");
				out.close();
				return;
			}
		}else{
			filterChain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
