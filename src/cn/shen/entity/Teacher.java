package cn.shen.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * <p>
 * 教师
 * </p>
 */
@TableName("teacher")
public class Teacher extends Model<Teacher> {

    private static final long serialVersionUID=1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    @TableField("userName")
    private String userName;

    /**
     * 密码
     */
    @TableField("passWord")
    private String passWord;

    /**
     * 真实姓名
     */
    @TableField("name")
    private String name;

    /**
     * 学科
     */
    @TableField("subject")
    private String subject;

    /**
     * 年级
     */
    @TableField("grade")
    private String grade;

    /**
     * 家教时间
     */
    @TableField("time")
    private String time;


    /**
     * 家教心得
     */
    @TableField("content")
    private String content;

    /**
     * 家教一次的价格
     */
    @TableField("price")
    private Integer price;

    /**
     * 电话
     */
    @TableField("phone")
    private String phone;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 证件照
     */
    @TableField("pic")
    private String pic;

    /**
     * 身份
     */
    @TableField("role")
    private Integer role;
    
    /**
     * 价格级别
     */
    @TableField("price_jibie")
    private Integer price_jibie;
    
    /**
     * 是否优秀教师
     */
    @TableField("isgood")
    private Integer isgood;
    
    /**
     * 是否新教师
     */
    @TableField("isnew")
    private Integer isnew;

    /**
     * 是否有通知
     */
    @TableField("isnotice")
    private Integer isnotice;
    
    /**
     * 是否有通知
     */
    @TableField("isxinxi")
    private Integer isxinxi;
    
    /**
     * 是否删除
     */
    @TableField("isdel")
    private Integer isdel;

    
    
    public Integer getIsxinxi() {
		return isxinxi;
	}

	public void setIsxinxi(Integer isxinxi) {
		this.isxinxi = isxinxi;
	}

	public Integer getIsdel() {
		return isdel;
	}

	public void setIsdel(Integer isdel) {
		this.isdel = isdel;
	}

	public Integer getIsnotice() {
		return isnotice;
	}

	public void setIsnotice(Integer isnotice) {
		this.isnotice = isnotice;
	}

	public Integer getIsnew() {
		return isnew;
	}

	public void setIsnew(Integer isnew) {
		this.isnew = isnew;
	}

	public Integer getIsgood() {
		return isgood;
	}

	public void setIsgood(Integer isgood) {
		this.isgood = isgood;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Integer getPrice_jibie() {
		return price_jibie;
	}

	public void setPrice_jibie(Integer price_jibie) {
		this.price_jibie = price_jibie;
	}

	public Integer getId() {
        return id;
    }

    public Teacher setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public Teacher setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassWord() {
        return passWord;
    }

    public Teacher setPassWord(String passWord) {
        this.passWord = passWord;
        return this;
    }

    public String getName() {
        return name;
    }

    public Teacher setName(String name) {
        this.name = name;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public Teacher setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getGrade() {
        return grade;
    }

    public Teacher setGrade(String grade) {
        this.grade = grade;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Teacher setContent(String content) {
        this.content = content;
        return this;
    }

    public Integer getPrice() {
        return price;
    }

    public Teacher setPrice(Integer price) {
        this.price = price;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Teacher setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Teacher setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPic() {
        return pic;
    }

    public Teacher setPic(String pic) {
        this.pic = pic;
        return this;
    }

    public Integer getRole() {
        return role;
    }

    public Teacher setRole(Integer role) {
        this.role = role;
        return this;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Teacher{" +
        "id=" + id +
        ", userName=" + userName +
        ", passWord=" + passWord +
        ", name=" + name +
        ", subject=" + subject +
        ", grade=" + grade +
        ", time=" + time +
        ", content=" + content +
        ", price=" + price +
        ", phone=" + phone +
        ", email=" + email +
        ", pic=" + pic +
        ", price_jibie=" + price_jibie +
        ", isgood=" + isgood +
        ", isnew=" + isnew +
        ", isnotice=" + isnotice +
        ", role=" + role +
        ", isdel=" + isdel +
        ", isxinxi=" + isxinxi +
        "}";
    }
}
