package cn.shen.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * <p>
 * 留言
 * </p>

 */
@TableName("message")
public class Message extends Model<Message> {

    private static final long serialVersionUID=1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 留言的用户ID
     */
    @TableField("uid")
    private Integer uid;

    /**
     * 被留言老师的ID
     */
    @TableField("to_tid")
    private Integer to_tid;
    
    /**
     * 被留言的管理员ID
     */
    @TableField("to_aid")
    private Integer to_aid;


    /**
     * 留言内容
     */
    @TableField("content")
    private String content;

    /**
     * 留言时间
     */
    @TableField("time")
    private String time;

    /**
     * 是否删除
     */
    @TableField("isdel")
    private Integer isdel;
    
    /**
     * 是否回复
     */
    @TableField("isreply")
    private Integer isreply;

    @TableField(exist = false)
    private String userName;
    
    @TableField(exist = false)
    private String teacherName;

    @TableField(exist = false)
    private String adminName;
    
    
    
    public Integer getTo_tid() {
		return to_tid;
	}

	public void setTo_tid(Integer to_tid) {
		this.to_tid = to_tid;
	}

	public Integer getTo_aid() {
		return to_aid;
	}

	public void setTo_aid(Integer to_aid) {
		this.to_aid = to_aid;
	}

	public Integer getIsreply() {
		return isreply;
	}

	public void setIsreply(Integer isreply) {
		this.isreply = isreply;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getId() {
        return id;
    }

    public Message setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getUid() {
        return uid;
    }

    public Message setUid(Integer uid) {
        this.uid = uid;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Message setContent(String content) {
        this.content = content;
        return this;
    }

    public String getTime() {
        return time;
    }

    public Message setTime(String time) {
        this.time = time;
        return this;
    }

    public Integer getIsdel() {
        return isdel;
    }

    public Message setIsdel(Integer isdel) {
        this.isdel = isdel;
        return this;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Message{" +
        "id=" + id +
        ", uid=" + uid +
        ", to_tid=" + to_tid +
        ", to_aid=" + to_aid +
        ", content=" + content +
        ", time=" + time +
        ", isdel=" + isdel +
        ", isreply=" + isreply +
        "}";
    }
}
