package cn.shen.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 */
@TableName("reply")
public class Reply extends Model<Reply> {

    private static final long serialVersionUID=1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 留言ID
     */
    @TableField("mid")
    private Integer mid;

    /**
     * 回复的老师ID
     */
    @TableField("rid")
    private Integer rid;
    
    /**
     * 回复的系统管理员ID
     */
    @TableField("uid")
    private Integer uid;

    /**
     * 回复内容
     */
    @TableField("content")
    private String content;

    /**
     * 回复时间
     */
    @TableField("time")
    private String time;

    /**
     * 是否删除
     */
    @TableField("isdel")
    private Integer isdel;
    
    /**
     * 是否是管理员回复
     */
    @TableField("isadmin")
    private Integer isadmin;

    @TableField(exist = false)
    private String userName;
    
    @TableField(exist = false)
    private String replyName;
    
    
    public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getIsadmin() {
		return isadmin;
	}

	public void setIsadmin(Integer isadmin) {
		this.isadmin = isadmin;
	}

	public String getReplyName() {
		return replyName;
	}

	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getId() {
        return id;
    }

    public Reply setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getMid() {
        return mid;
    }

    public Reply setMid(Integer mid) {
        this.mid = mid;
        return this;
    }

    public Integer getRid() {
        return rid;
    }

    public Reply setRid(Integer rid) {
        this.rid = rid;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Reply setContent(String content) {
        this.content = content;
        return this;
    }

    public String getTime() {
        return time;
    }

    public Reply setTime(String time) {
        this.time = time;
        return this;
    }

    public Integer getIsdel() {
        return isdel;
    }

    public Reply setIsdel(Integer isdel) {
        this.isdel = isdel;
        return this;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Reply{" +
        "id=" + id +
        ", mid=" + mid +
        ", rid=" + rid +
        ", uid=" + uid +
        ", content=" + content +
        ", time=" + time +
        ", isdel=" + isdel +
        ", isadmin=" + isadmin +
        "}";
    }
}
