package cn.shen.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 */
@TableName("need")
public class Need extends Model<Need> {

    private static final long serialVersionUID=1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 发布需求的用户ID
     */
    @TableField("uid")
    private Integer uid;

    /**
     * 需求的学科
     */
    @TableField("subject")
    private String subject;

    /**
     * 需求的年级
     */
    @TableField("grade")
    private String grade;

    /**
     * 上课时间段
     */
    @TableField("studyhour")
    private String studyhour;

    /**
     * 发布时间
     */
    @TableField("time")
    private String time;

    /**
     * 电话
     */
    @TableField("phone")
    private String phone;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;
    
    /**
     * 价格
     */
    @TableField("price")
    private Integer price;
    
    /**
     * 价格区间
     */
    @TableField("price_jibie")
    private Integer price_jibie;
    
    /**
     * 是否删除
     */
    @TableField("isdel")
    private Integer isdel;
    
    @TableField(exist = false)
    private String userName;


    public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getPrice_jibie() {
		return price_jibie;
	}

	public void setPrice_jibie(Integer price_jibie) {
		this.price_jibie = price_jibie;
	}

	public Integer getIsdel() {
		return isdel;
	}

	public void setIsdel(Integer isdel) {
		this.isdel = isdel;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getId() {
        return id;
    }

    public Need setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getUid() {
        return uid;
    }

    public Need setUid(Integer uid) {
        this.uid = uid;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public Need setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getGrade() {
        return grade;
    }

    public Need setGrade(String grade) {
        this.grade = grade;
        return this;
    }

    public String getStudyhour() {
        return studyhour;
    }

    public Need setStudyhour(String studyhour) {
        this.studyhour = studyhour;
        return this;
    }

    public String getTime() {
        return time;
    }

    public Need setTime(String time) {
        this.time = time;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Need setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Need setEmail(String email) {
        this.email = email;
        return this;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Need{" +
        "id=" + id +
        ", uid=" + uid +
        ", subject=" + subject +
        ", grade=" + grade +
        ", studyhour=" + studyhour +
        ", time=" + time +
        ", phone=" + phone +
        ", email=" + email +
        ", price=" + price +
        ", price_jibie=" + price_jibie +
        ", isdel=" + isdel +
        "}";
    }
}
