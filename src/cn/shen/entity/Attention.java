package cn.shen.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * <p>
 * 关注
 * </p>
 */
@TableName("attention")
public class Attention extends Model<Attention> {

	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;

	/**
	 * 用户的ID
	 */
	@TableField("uid")
	private Integer uid;

	/**
	 * 被关注老师的ID
	 */
	@TableField("btid")
	private Integer btid;

	/**
	 * 老师的ID
	 */
	@TableField("tid")
	private Integer tid;

	/**
	 * 被关注用户的ID
	 */
	@TableField("buid")
	private Integer buid;

	/**
	 * 关注状态
	 */
	@TableField("status")
	private Integer status;

	/**
	 * 关注时间
	 */
	@TableField("time")
	private String time;

	/**
	 * 是否删除
	 */
	@TableField("isdel")
	private Integer isdel;

	@TableField(exist = false)
	private String subject;

	@TableField(exist = false)
	private String grade;

	@TableField(exist = false)
	private String teacherName;

	@TableField(exist = false)
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Integer getId() {
		return id;
	}

	public Attention setId(Integer id) {
		this.id = id;
		return this;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public Integer getBtid() {
		return btid;
	}

	public void setBtid(Integer btid) {
		this.btid = btid;
	}

	public Integer getTid() {
		return tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public Integer getBuid() {
		return buid;
	}

	public void setBuid(Integer buid) {
		this.buid = buid;
	}

	public Integer getStatus() {
		return status;
	}

	public Attention setStatus(Integer status) {
		this.status = status;
		return this;
	}

	public String getTime() {
		return time;
	}

	public Attention setTime(String time) {
		this.time = time;
		return this;
	}

	public Integer getIsdel() {
		return isdel;
	}

	public Attention setIsdel(Integer isdel) {
		this.isdel = isdel;
		return this;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Attention{" + "id=" + id + ", uid=" + uid + ", btid=" + btid + ", tid=" + tid + ", buid=" + buid
				+ ", status=" + status + ", time=" + time + ", isdel=" + isdel + "}";
	}
}
