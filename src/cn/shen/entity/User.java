package cn.shen.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

/**
 * <p>
 * 用户
 * </p>
 */
@TableName("user")
public class User extends Model<User> {

    private static final long serialVersionUID=1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名
     */
    @TableField("userName")
    private String userName;

    /**
     * 密码
     */
    @TableField("passWord")
    private String passWord;

    /**
     * 真实姓名
     */
    @TableField("name")
    private String name;

    /**
     * 学科
     */
    @TableField("subject")
    private String subject;

    /**
     * 年级
     */
    @TableField("grade")
    private String grade;

    /**
     * 家教时间
     */
    @TableField("time")
    private String time;

    /**
     * 电话
     */
    @TableField("phone")
    private String phone;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 证件照
     */
    @TableField("pic")
    private String pic;

    /**
     * 身份
     */
    @TableField("role")
    private Integer role;

    /**
     * 是否有通知
     */
    @TableField("isnotice")
    private Integer isnotice;
    
    /**
     * 是否有通知
     */
    @TableField("isdel")
    private Integer isdel;
    

    public Integer getIsdel() {
		return isdel;
	}

	public void setIsdel(Integer isdel) {
		this.isdel = isdel;
	}

	public Integer getIsnotice() {
		return isnotice;
	}

	public void setIsnotice(Integer isnotice) {
		this.isnotice = isnotice;
	}

	public Integer getId() {
        return id;
    }

    public User setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public User setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassWord() {
        return passWord;
    }

    public User setPassWord(String passWord) {
        this.passWord = passWord;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public User setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getGrade() {
        return grade;
    }

    public User setGrade(String grade) {
        this.grade = grade;
        return this;
    }

    public String getTime() {
        return time;
    }

    public User setTime(String time) {
        this.time = time;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public User setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPic() {
        return pic;
    }

    public User setPic(String pic) {
        this.pic = pic;
        return this;
    }

    public Integer getRole() {
        return role;
    }

    public User setRole(Integer role) {
        this.role = role;
        return this;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "User{" +
        "id=" + id +
        ", userName=" + userName +
        ", passWord=" + passWord +
        ", name=" + name +
        ", subject=" + subject +
        ", grade=" + grade +
        ", time=" + time +
        ", phone=" + phone +
        ", email=" + email +
        ", pic=" + pic +
        ", role=" + role +
        ", isnotice=" + isnotice +
        ", isdel=" + isdel +
        "}";
    }
}
