package cn.shen.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 */
@TableName("orders")
public class Orders extends Model<Orders> {

    private static final long serialVersionUID=1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 教员ID
     */
    @TableField("tid")
    private Integer tid;

    /**
     * 用户ID
     */
    @TableField("uid")
    private Integer uid;
    
    /**
     * 发布需求ID
     */
    @TableField("nid")
    private Integer nid;

    /**
     * 学科
     */
    @TableField("subject")
    private String subject;

    /**
     * 年级
     */
    @TableField("grade")
    private String grade;

    /**
     * 上课次数
     */
    @TableField("times")
    private Integer times;

    /**
     * 订单创建时间
     */
    @TableField("time")
    private String time;

    /**
     * 订单价格
     */
    @TableField("price")
    private Integer price;

    /**
     * 订单是否完成
     */
    @TableField("isend")
    private Integer isend;

    /**
     * 是否试教
     */
    @TableField("istry")
    private Integer istry;

    /**
     * 订单评价
     */
    @TableField("evaluation")
    private String evaluation;

    /**
     * 评价时间
     */
    @TableField("evaluationtime")
    private String evaluationtime;

    /**
     * 是否删除
     */
    @TableField("isdel")
    private Integer isdel;
    
    /**
     * 用户是否同意
     */
    @TableField("isUagree")
    private Integer isUagree;
    
    /**
     * 老师是否同意
     */
    @TableField("isTagree")
    private Integer isTagree;
    
    /**
     * 是否同意
     */
    @TableField("isevaluation")
    private Integer isevaluation;
    
    /**
     * 上课时间
     */
    @TableField("classtime")
    private String classtime;
    
    /**
     * 订单星级
     */
    @TableField("level")
    private Integer level;
    
    @TableField(exist = false)
    private String userName;
    
    @TableField(exist = false)
    private String teacherName;

    

    
    public Integer getNid() {
		return nid;
	}

	public void setNid(Integer nid) {
		this.nid = nid;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getClasstime() {
		return classtime;
	}

	public void setClasstime(String classtime) {
		this.classtime = classtime;
	}

	public Integer getIsUagree() {
		return isUagree;
	}

	public void setIsUagree(Integer isUagree) {
		this.isUagree = isUagree;
	}

	public Integer getIsTagree() {
		return isTagree;
	}

	public void setIsTagree(Integer isTagree) {
		this.isTagree = isTagree;
	}

	public Integer getIsevaluation() {
		return isevaluation;
	}

	public void setIsevaluation(Integer isevaluation) {
		this.isevaluation = isevaluation;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public Integer getId() {
        return id;
    }

    public Orders setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getTid() {
        return tid;
    }

    public Orders setTid(Integer tid) {
        this.tid = tid;
        return this;
    }

    public Integer getUid() {
        return uid;
    }

    public Orders setUid(Integer uid) {
        this.uid = uid;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public Orders setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getGrade() {
        return grade;
    }

    public Orders setGrade(String grade) {
        this.grade = grade;
        return this;
    }

    public Integer getTimes() {
        return times;
    }

    public Orders setTimes(Integer times) {
        this.times = times;
        return this;
    }

    public String getTime() {
        return time;
    }

    public Orders setTime(String time) {
        this.time = time;
        return this;
    }

    public Integer getPrice() {
        return price;
    }

    public Orders setPrice(Integer price) {
        this.price = price;
        return this;
    }

    public Integer getIsend() {
        return isend;
    }

    public Orders setIsend(Integer isend) {
        this.isend = isend;
        return this;
    }

    public Integer getIstry() {
        return istry;
    }

    public Orders setIstry(Integer istry) {
        this.istry = istry;
        return this;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public Orders setEvaluation(String evaluation) {
        this.evaluation = evaluation;
        return this;
    }

    public String getEvaluationtime() {
        return evaluationtime;
    }

    public Orders setEvaluationtime(String evaluationtime) {
        this.evaluationtime = evaluationtime;
        return this;
    }

    public Integer getIsdel() {
        return isdel;
    }

    public Orders setIsdel(Integer isdel) {
        this.isdel = isdel;
        return this;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Order{" +
        "id=" + id +
        ", tid=" + tid +
        ", uid=" + uid +
        ", subject=" + subject +
        ", grade=" + grade +
        ", times=" + times +
        ", time=" + time +
        ", price=" + price +
        ", isend=" + isend +
        ", istry=" + istry +
        ", isTagree=" + isTagree +
        ", isUagree=" + isUagree +
        ", isevaluation=" + isevaluation +
        ", evaluation=" + evaluation +
        ", evaluationtime=" + evaluationtime +
        ", isdel=" + isdel +
        ", classtime=" + classtime +
        ", level=" + level +
        "}";
    }
}
