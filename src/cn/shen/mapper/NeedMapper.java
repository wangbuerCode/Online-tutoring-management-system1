package cn.shen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.shen.entity.Need;


public interface NeedMapper extends BaseMapper<Need> {

}
