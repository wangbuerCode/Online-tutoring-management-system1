package cn.shen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.shen.entity.Chart;

public interface ChartMapper extends BaseMapper<Chart> {

}
