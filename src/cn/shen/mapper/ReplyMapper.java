package cn.shen.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.shen.entity.Reply;


public interface ReplyMapper extends BaseMapper<Reply> {

}
