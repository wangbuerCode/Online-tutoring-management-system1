package cn.shen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.shen.entity.User;


public interface UserMapper extends BaseMapper<User> {

}
