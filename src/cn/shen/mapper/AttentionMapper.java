package cn.shen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.shen.entity.Attention;

public interface AttentionMapper extends BaseMapper<Attention> {

}
