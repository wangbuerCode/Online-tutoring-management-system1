package cn.shen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.shen.entity.Teacher;


public interface TeacherMapper extends BaseMapper<Teacher> {

}
