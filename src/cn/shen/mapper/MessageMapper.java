package cn.shen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.shen.entity.Message;

public interface MessageMapper extends BaseMapper<Message> {

}
