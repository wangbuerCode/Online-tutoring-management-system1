package cn.shen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.shen.entity.Orders;

public interface OrdersMapper extends BaseMapper<Orders> {

}
